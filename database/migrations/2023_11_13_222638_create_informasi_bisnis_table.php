<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('informasi_bisnis', function (Blueprint $table) {
            $table->id();
            $table->string('nama_bisnis')->nullable();
            $table->string('alamat')->nullable();
            $table->string('no_tax')->nullable();
            $table->string('website')->nullable();
            $table->string('email')->nullable();
            $table->string('no_handphone');
            $table->string('logo')->nullable();
            $table->string('jabatan')->nullable();
            $table->foreignId('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('informasi_bisnis');
    }
};
