const tabelBody = document.getElementById('tabelBody')
const tabelRowPem = Array.from(document.querySelectorAll('#tabelRowPem'))
const navPagination = document.getElementById('nav-pagination')
const periodeBulanDropdown = document.getElementById('periodeBulanDropdownButton')


const elementJumlahTransaksi = document.getElementById('jumlah-transaksi-laporan')

let elemenLoading = `<tr class="" id="elementLoading">
                            <td colspan="6" class="w-full">
                                <div role="status" class="flex items-center justify-center py-6">
                                <svg aria-hidden="true" class="w-8 h-8 text-gray-200 animate-spin dark:text-gray-600 fill-blue-600" viewBox="0 0 100 101" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z" fill="currentColor"/>
                                <path d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z" fill="currentFill"/>
                            </svg>
                            <span class="sr-only">Loading...</span>
                                </div>
                            </td>
                        </tr>`


// function getJumlahTransaksiByMonth(){
//    return elementJumlahTransaksi;
// }


async function acttionFilter(target) {
    document.getElementById('pagination').remove();
    tabelRowPem.forEach(tr => {
        while (tr.firstChild) {
            tr.removeChild(tr.firstChild)
        }
    })

    tabelBody.innerHTML = elemenLoading;


    fetch('jumlah-transaksi-by-kategori?id=' + target.getAttribute('data-id') + '&jenis_transaksi_id=' + target.getAttribute('data-id-2'))
        .then(response => response.text())
        .then(data => {
            elementJumlahTransaksi.textContent = 'Rp ' + data
            console.log(data);
        })


    const response = await fetch('get_pemasukan_by_kategori_transaksi_id/?id=' + target.getAttribute('data-id') + '&jenis_transaksi_id=' + target.getAttribute('data-id-2'))
    let data = await response.json()
    let dataResult = data['data']
    tabelBody.innerHTML = '';
    dataResult.data.forEach((item, index) => {
        let element = `
                        <th scope="row"
                            class="px-4 py-3 font-medium text-gray-900 whitespace-nowrap dark:text-white">${item.no_transaksi}</th>
                        <td class="px-4 py-3">${item.tanggal}</td>
                        <td class="px-4 py-3">${item.kategori_transaksi.nama}</td>
                        <td class="px-4 py-3">${item.suppliers_or_customers?.nama_bisnis ?? '--'}</td>
                        <td class="px-4 py-3">${item.deskripsi ?? '--'}</td>
                        <td class="px-4 py-3">Rp ${item.jumlah.toLocaleString()}</td>
                        `

         // Buat elemen baris tabel baru
        let newRow = document.createElement('tr');
        newRow.className = 'border-b dark:border-gray-700'; // Setel kelas sesuai kebutuhan
        newRow.id = 'tabelRowPem'; // Setel ID sesuai kebutuhan
        newRow.innerHTML = element;

        // Tambahkan baris baru ke dalam tabel
        tabelBody.appendChild(newRow);
        tabelRowPem[index].innerHTML = element

        console.log(data['data']);

        const elementPagination = `<div id="pagination" class="grid grid-cols-2 md:flex items-center gap-3">
                                        <button ${data['data'].prev_page_url == null ? 'disabled' : ''} id="prev-url" data-url="${data['data'].prev_page_url + '&id=' +data['id']}" class="flex items-center justify-center px-4 h-10 text-base font-medium text-gray-500 bg-white border border-gray-300 rounded-lg hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white">
                                        Sebelumnya
                                        </button>

                                        <button id="next-url" data-url="${data['data'].next_page_url + '&id=' +data['id']}" class="flex items-center justify-center px-4 h-10 text-base font-medium text-gray-500 bg-white border border-gray-300 rounded-lg hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white">
                                        Selanjutnya
                                        </button>
                                        <span class="text-sm flex gap-1 text-gray-700 dark:text-gray-400">
                                        Menampilkan<span id="data-from" class="font-semibold text-gray-900 dark:text-white">${data['data'].from}</span> hingga <span id="data-to" class="font-semibold text-gray-900 dark:text-white">${data['data'].to}</span> dari <span id="data-total" class="font-semibold text-gray-900 dark:text-white">${data['data'].total}</span> Enteri
                                        </span>
                                    </div>`

        navPagination.innerHTML = elementPagination



        const nextPagination = document.getElementById('next-url')
        const prevPagination = document.getElementById('prev-url')
        const dataFrom = document.getElementById('data-from')
        const dataTo = document.getElementById('data-to')
        const dataTotal = document.getElementById('data-total')

        nextPagination.addEventListener('click', async function () {
            tabelRowPem.forEach(tr => {
                while (tr.firstChild) {
                    tr.removeChild(tr.firstChild)
                }
            })

            tabelBody.innerHTML = elemenLoading;

            let link = nextPagination.getAttribute('data-url');
            console.log(link);
            const responsePagination = await fetch(link)
            const dataPagination = await responsePagination.json()
            tabelBody.innerHTML = '';
            dataPagination['data'].data.forEach((item, index) => {
                let element = `
                        <th scope="row"
                            class="px-4 py-3 font-medium text-gray-900 whitespace-nowrap dark:text-white">${item.no_transaksi}</th>
                        <td class="px-4 py-3">${item.tanggal}</td>
                        <td class="px-4 py-3">${item.kategori_transaksi.nama}</td>
                        <td class="px-4 py-3">${item.suppliers_or_customers?.nama_bisnis ?? '--'}</td>
                        <td class="px-4 py-3">${item.deskripsi ?? '--'}</td>
                        <td class="px-4 py-3">Rp ${item.jumlah.toLocaleString()}</td>
                        `


                let newRow = document.createElement('tr');
                newRow.className = 'border-b dark:border-gray-700'; // Setel kelas sesuai kebutuhan
                newRow.id = 'tabelRowPem'; // Setel ID sesuai kebutuhan
                newRow.innerHTML = element;

                // Tambahkan baris baru ke dalam tabel
                tabelBody.appendChild(newRow);
                tabelRowPem[index].innerHTML = element
            })
            nextPagination.setAttribute('data-url', dataPagination['data'].next_page_url + '&id=' + dataPagination['id'])
            prevPagination.setAttribute('data-url', dataPagination['data'].prev_page_url + '&id=' + dataPagination['id'])
            prevPagination.disabled = dataPagination['data'].prev_page_url == null ? true : false
            nextPagination.disabled = dataPagination['data'].next_page_url == null ? true : false
            dataFrom.textContent  = dataPagination['data'].from
            dataTo.textContent = dataPagination['data'].to
            dataTotal.textContent = dataPagination['data'].total
        })

        prevPagination.addEventListener('click', async function () {
            tabelRowPem.forEach(tr => {
                while (tr.firstChild) {
                    tr.removeChild(tr.firstChild)
                }
            })

            tabelBody.innerHTML = elemenLoading;

            let link = prevPagination.getAttribute('data-url');
            console.log(link);
            const responsePagination = await fetch(link)
            const dataPagination = await responsePagination.json()
            tabelBody.innerHTML = '';
            dataPagination['data'].data.forEach((item, index) => {
                let element = `
                        <th scope="row"
                            class="px-4 py-3 font-medium text-gray-900 whitespace-nowrap dark:text-white">${item.no_transaksi}</th>
                        <td class="px-4 py-3">${item.tanggal}</td>
                        <td class="px-4 py-3">${item.kategori_transaksi.nama}</td>
                        <td class="px-4 py-3">${item.suppliers_or_customers?.nama_bisnis ?? '--'}</td>
                        <td class="px-4 py-3">${item.deskripsi ?? '--'}</td>
                        <td class="px-4 py-3">Rp ${item.jumlah.toLocaleString()}</td>
                        `

                let newRow = document.createElement('tr');
                newRow.className = 'border-b dark:border-gray-700'; // Setel kelas sesuai kebutuhan
                newRow.id = 'tabelRowPem'; // Setel ID sesuai kebutuhan
                newRow.innerHTML = element;

                // Tambahkan baris baru ke dalam tabel
                tabelBody.appendChild(newRow);
                tabelRowPem[index].innerHTML = element
            })
            prevPagination.setAttribute('data-url', dataPagination['data'].prev_page_url + '&id=' + dataPagination['id'])
            nextPagination.setAttribute('data-url', dataPagination['data'].next_page_url + '&id=' + dataPagination['id'])
            prevPagination.disabled = dataPagination['data'].prev_page_url == null ? true : false
            dataFrom.textContent  = dataPagination['data'].from
            dataTo.textContent = dataPagination['data'].to
            dataTotal.textContent = dataPagination['data'].total
        })
    });
}

async function acttionPeriode(target) {
    const linkPrint = document.getElementById('linkPrint')
    const linkExcel = document.getElementById('linkExcel')
    const linkPrintPengeluaran = document.getElementById('linkPrintPengeluaran')
    const linkExcelPengeluaran = document.getElementById('linkExcelPengeluaran')

    document.getElementById('pagination').remove();
    tabelBody.innerHTML = elemenLoading;

    // const elementJumlahTransaksi = document.getElementById('jumlah-transaksi-laporan')

    if (linkPrint != null || linkExcel != null) {
        linkPrint.setAttribute('href', `/pdf_laporan_pemasukan/?id=${target.getAttribute('data-id')}`)
        linkExcel.setAttribute('href', `/pemasukan_xlsx/?id=${target.getAttribute('data-id')}`)
    }
    if (linkPrintPengeluaran != null || linkExcelPengeluaran != null) {
        linkPrintPengeluaran.setAttribute('href', `/pdf_laporan_pengeluaran/?id=${target.getAttribute('data-id')}`)
        linkExcelPengeluaran.setAttribute('href', `/pengeluaran_xlsx/?id=${target.getAttribute('data-id')}`)
    }
    tabelRowPem.forEach(tr => {
        while (tr.firstChild) {
            tr.removeChild(tr.firstChild)
        }
    })

    fetch('jumlah-transaksi-by-month?id=' + target.getAttribute('data-id') + '&jenis_transaksi_id=' + target.getAttribute('data-id-2'))
        .then(response => response.text())
        .then(data => {
            elementJumlahTransaksi.textContent = 'Rp ' + data
        })

    // let dataResponse =
    let response = await fetch('/get_pemasukan_by_month/?id=' + target.getAttribute('data-id') + '&jenis_transaksi_id=' + target.getAttribute('data-id-2'))
    let data = await response.json();
    let dataResult = data['data']
    tabelBody.innerHTML = '';
    dataResult.data.forEach((item, index) => {
        let element = `
                        <th scope="row"
                            class="px-4 py-3 font-medium text-gray-900 whitespace-nowrap dark:text-white">${item.no_transaksi}</th>
                        <td class="px-4 py-3">${item.tanggal}</td>
                        <td class="px-4 py-3">${item.kategori_transaksi.nama}</td>
                        <td class="px-4 py-3">${item.suppliers_or_customers?.nama_bisnis ?? '--'}</td>
                        <td class="px-4 py-3">${item.deskripsi ?? '--'}</td>
                        <td class="px-4 py-3">Rp ${item.jumlah.toLocaleString()}</td>
                        `

        let newRow = document.createElement('tr');
        newRow.className = 'border-b dark:border-gray-700'; // Setel kelas sesuai kebutuhan
        newRow.id = 'tabelRowPem'; // Setel ID sesuai kebutuhan
        newRow.innerHTML = element;

        // Tambahkan baris baru ke dalam tabel
        tabelBody.appendChild(newRow);

        tabelRowPem[index].innerHTML = element
    });


    const elementPagination = `<div id="pagination" class="grid grid-cols-2 md:flex items-center gap-3">
                                <button id="prev-url" data-url="${data['data'].prev_page_url + '&id=' +data['id'] + '&jenis_transaksi_id=' + data['jenis_transaksi_id']}" class="flex items-center justify-center px-4 h-10 text-base font-medium text-gray-500 bg-white border border-gray-300 rounded-lg hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white">
                                Sebelumnya
                                </button>

                                <button id="next-url" data-url="${data['data'].next_page_url + '&id=' +data['id'] + '&jenis_transaksi_id=' + data['jenis_transaksi_id']}" class="flex items-center justify-center px-4 h-10 text-base font-medium text-gray-500 bg-white border border-gray-300 rounded-lg hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white">
                                Selanjutnya
                                </button>
                                <span class="text-sm flex gap-1 text-gray-700 dark:text-gray-400">
                                Menampilkan<span id="data-from" class="font-semibold text-gray-900 dark:text-white">${data['data'].from}</span> hingga <span id="data-to" class="font-semibold text-gray-900 dark:text-white">${data['data'].to}</span> dari <span id="data-total" class="font-semibold text-gray-900 dark:text-white">${data['data'].total}</span> Enteri
                                </span>
                            </div>`

    navPagination.innerHTML = elementPagination

    const nextPagination = document.getElementById('next-url')
    const prevPagination = document.getElementById('prev-url')
    const dataFrom = document.getElementById('data-from')
    const dataTo = document.getElementById('data-to')
    const dataTotal = document.getElementById('data-total')


    nextPagination.addEventListener('click', async function () {
        let link = nextPagination.getAttribute('data-url');
        tabelBody.innerHTML = elemenLoading;
        console.log(link);
        const responsePagination = await fetch(link)
        const dataPagination = await responsePagination.json()

        tabelBody.innerHTML = ''
        dataPagination['data'].data.forEach((item, index) => {
            let element = `
                        <th scope="row"
                            class="px-4 py-3 font-medium text-gray-900 whitespace-nowrap dark:text-white">${item.no_transaksi}</th>
                        <td class="px-4 py-3">${item.tanggal}</td>
                        <td class="px-4 py-3">${item.kategori_transaksi.nama}</td>
                        <td class="px-4 py-3">${item.suppliers_or_customers?.nama_bisnis ?? '--'}</td>
                        <td class="px-4 py-3">${item.deskripsi ?? '--'}</td>
                        <td class="px-4 py-3">Rp ${item.jumlah}</td>
                        `


            let newRow = document.createElement('tr');
            newRow.className = 'border-b dark:border-gray-700'; // Setel kelas sesuai kebutuhan
            newRow.id = 'tabelRowPem'; // Setel ID sesuai kebutuhan
            newRow.innerHTML = element;
             // Tambahkan baris baru ke dalam tabel
            tabelBody.appendChild(newRow);
            tabelRowPem[index].innerHTML = element
        })
        nextPagination.setAttribute('data-url', dataPagination['data'].next_page_url + '&id=' +data['id'] + '&jenis_transaksi_id=' + data['jenis_transaksi_id'])
        prevPagination.setAttribute('data-url', dataPagination['data'].prev_page_url + '&id=' +data['id'] + '&jenis_transaksi_id=' + data['jenis_transaksi_id'])
        prevPagination.disabled = dataPagination['data'].prev_page_url == null ? true : false
        nextPagination.disabled = dataPagination['data'].next_page_url == null ? true : false
        dataFrom.textContent  = dataPagination['data'].from
        dataTo.textContent = dataPagination['data'].to
        dataTotal.textContent = dataPagination['data'].total

    })

    prevPagination.addEventListener('click', async function () {
        let link = prevPagination.getAttribute('data-url');
        console.log(link);
        const responsePagination = await fetch(link)
        const dataPagination = await responsePagination.json()

        dataPagination['data'].data.forEach((item, index) => {
            let element = `
                        <th scope="row"
                            class="px-4 py-3 font-medium text-gray-900 whitespace-nowrap dark:text-white">${item.no_transaksi}</th>
                        <td class="px-4 py-3">${item.tanggal}</td>
                        <td class="px-4 py-3">${item.kategori_transaksi.nama}</td>
                        <td class="px-4 py-3">${item.suppliers_or_customers?.nama_bisnis ?? '--'}</td>
                        <td class="px-4 py-3">${item.deskripsi ?? '--'}</td>
                        <td class="px-4 py-3">Rp ${item.jumlah}</td>
                        `
        let newRow = document.createElement('tr');
        newRow.className = 'border-b dark:border-gray-700'; // Setel kelas sesuai kebutuhan
        newRow.id = 'tabelRowPem'; // Setel ID sesuai kebutuhan
        newRow.innerHTML = element;
                            // Tambahkan baris baru ke dalam tabel
        tabelBody.appendChild(newRow);
        tabelRowPem[index].innerHTML = element
        })
        prevPagination.setAttribute('data-url', dataPagination['data'].prev_page_url + '&id=' +data['id'] + '&jenis_transaksi_id=' + data['jenis_transaksi_id'])
        nextPagination.setAttribute('data-url', dataPagination['data'].next_page_url + '&id=' +data['id'] + '&jenis_transaksi_id=' + data['jenis_transaksi_id'])
        prevPagination.disabled = dataPagination['data'].prev_page_url == null ? true : false
        dataFrom.textContent  = dataPagination['data'].from
        dataTo.textContent = dataPagination['data'].to
        dataTotal.textContent = dataPagination['data'].total
    })

}

async function yearsFilter(target) {
    document.getElementById('pagination').remove();
    tabelRowPem.forEach(tr => {
        while (tr.firstChild) {
            tr.removeChild(tr.firstChild)
        }
    })

    const id1 = target.getAttribute('data-id');
    const id2 = target.getAttribute('data-id-2');


    fetch('get-jumlah-kategori-transaksi-by-years?id=' + target.getAttribute('data-id') + '&jenis_transaksi_id=' + target.getAttribute('data-id-2'))
        .then(response => response.text())
        .then(data => {
            elementJumlahTransaksi.textContent = 'Rp ' + data
        })


    const response = await fetch('get-kategori-transaksi-by-years/?id=' + target.getAttribute('data-id') + '&jenis_transaksi_id=' + target.getAttribute('data-id-2'))
    const data = await response.json()
    let dataResul = data['data']
    dataResul.data.forEach((item, index) => {
        let element = `
                        <th scope="row"
                            class="px-4 py-3 font-medium text-gray-900 whitespace-nowrap dark:text-white">${item.no_transaksi}</th>
                        <td class="px-4 py-3">${item.tanggal}</td>
                        <td class="px-4 py-3">${item.kategori_transaksi.nama}</td>
                        <td class="px-4 py-3">${item.suppliers_or_customers?.nama_bisnis ?? '--'}</td>
                        <td class="px-4 py-3">${item.deskripsi ?? '--'}</td>
                        <td class="px-4 py-3">Rp ${item.jumlah.toLocaleString()}</td>
                        `
        tabelRowPem[index].innerHTML = element


        const elementPagination = `<div id="pagination" class="grid grid-cols-2 md:flex items-center gap-3">
                                    <button id="prev-url" data-url="${data['data'].prev_page_url + '&id=' + data['id'] + '&jenis_transaksi_id=' + data['jenis_transaksi_id']}" class="flex items-center justify-center px-4 h-10 text-base font-medium text-gray-500 bg-white border border-gray-300 rounded-lg hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white">
                                    Sebelumnya
                                    </button>

                                    <button id="next-url" data-url="${data['data'].next_page_url + '&id=' + data['id'] + '&jenis_transaksi_id=' + data['jenis_transaksi_id']}" class="flex items-center justify-center px-4 h-10 ms-3 text-base font-medium text-gray-500 bg-white border border-gray-300 rounded-lg hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white">
                                    Selanjutnya
                                    </button>

                                    <span class="text-sm flex gap-1 text-gray-700 dark:text-gray-400">
                                    Menampilkan<span id="data-from" class="font-semibold text-gray-900 dark:text-white">${data['data'].from}</span> hingga <span id="data-to" class="font-semibold text-gray-900 dark:text-white">${data['data'].to}</span> dari <span id="data-total" class="font-semibold text-gray-900 dark:text-white">${data['data'].total}</span> Enteri
                                    </span>
                                </div>`

        navPagination.innerHTML = elementPagination



        const nextPagination = document.getElementById('next-url')
        const prevPagination = document.getElementById('prev-url')
        const dataFrom = document.getElementById('data-from')
        const dataTo = document.getElementById('data-to')
        const dataTotal = document.getElementById('data-total')

        nextPagination.addEventListener('click', async function () {
            tabelRowPem.forEach(tr => {
                while (tr.firstChild) {
                    tr.removeChild(tr.firstChild)
                }
            })
            let link = nextPagination.getAttribute('data-url');
            console.log(link);
            const responsePagination = await fetch(link)
            const dataPagination = await responsePagination.json()

            dataPagination['data'].data.forEach((item, index) => {
                let element = `
                        <th scope="row"
                            class="px-4 py-3 font-medium text-gray-900 whitespace-nowrap dark:text-white">${item.no_transaksi}</th>
                        <td class="px-4 py-3">${item.tanggal}</td>
                        <td class="px-4 py-3">${item.kategori_transaksi.nama}</td>
                        <td class="px-4 py-3">${item.suppliers_or_customers?.nama_bisnis ?? '--'}</td>
                        <td class="px-4 py-3">${item.deskripsi ?? '--'}</td>
                        <td class="px-4 py-3">Rp ${item.jumlah}</td>
                        `
                tabelRowPem[index].innerHTML = element
            })
            nextPagination.setAttribute('data-url', dataPagination['data'].next_page_url + '&id=' + data['id'] + '&jenis_transaksi_id=' + data['jenis_transaksi_id'])
            prevPagination.setAttribute('data-url', dataPagination['data'].prev_page_url + '&id=' + data['id'] + '&jenis_transaksi_id=' + data['jenis_transaksi_id'])
            dataFrom.textContent  = dataPagination['data'].from
            dataTo.textContent = dataPagination['data'].to
            dataTotal.textContent = dataPagination['data'].total

        })

        prevPagination.addEventListener('click', async function () {
            tabelRowPem.forEach(tr => {
                while (tr.firstChild) {
                    tr.removeChild(tr.firstChild)
                }
            })
            let link = prevPagination.getAttribute('data-url');
            console.log(link);
            const responsePagination = await fetch(link)
            const dataPagination = await responsePagination.json()

            dataPagination['data'].data.forEach((item, index) => {
                let element = `
                        <th scope="row"
                            class="px-4 py-3 font-medium text-gray-900 whitespace-nowrap dark:text-white">${item.no_transaksi}</th>
                        <td class="px-4 py-3">${item.tanggal}</td>
                        <td class="px-4 py-3">${item.kategori_transaksi.nama}</td>
                        <td class="px-4 py-3">${item.suppliers_or_customers?.nama_bisnis ?? '--'}</td>
                        <td class="px-4 py-3">${item.deskripsi ?? '--'}</td>
                        <td class="px-4 py-3">Rp ${item.jumlah}</td>
                        `
                tabelRowPem[index].innerHTML = element
            })
            prevPagination.setAttribute('data-url', dataPagination.prev_page_url + '&id=' + data['id'] + '&jenis_transaksi_id=' + data['jenis_transaksi_id'])
            nextPagination.setAttribute('data-url', dataPagination.next_page_url + '&id=' + data['id'] + '&jenis_transaksi_id=' + data['jenis_transaksi_id'])
            dataFrom.textContent  = dataPagination['data'].from
            dataTo.textContent = dataPagination['data'].to
            dataTotal.textContent = dataPagination['data'].total
        })
    });

    const request = await fetch('get-months-by-year?id=' + target.getAttribute('data-id'))
    const dataResponseMonth = await request.json();

    const contanerListItem = document.getElementById("container-list-item");

    // Bersihkan konten yang sudah ada di dalam contanerListItem
    contanerListItem.innerHTML = '';

    // Buat elemen <li> untuk "Semua"
    let listItemSemua = document.createElement('li');
    listItemSemua.onclick = function () {
        acttionPeriode(this);
    };
    listItemSemua.id = 'filterKategori';
    listItemSemua.setAttribute('data-id-2', target.getAttribute('data-id-2'));
    listItemSemua.setAttribute('data-id', 'all');

    // Buat elemen <a> di dalam <li> untuk "Semua"
    let linkSemua = document.createElement('a');
    linkSemua.className = 'block py-2 px-4 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white';
    linkSemua.innerText = 'Semua';

    // Sisipkan elemen <a> ke dalam elemen <li>
    listItemSemua.appendChild(linkSemua);

    // Sisipkan elemen <li> ke dalam container
    contanerListItem.appendChild(listItemSemua);

    // Buat elemen <li> untuk setiap item bulan lainnya
    dataResponseMonth.forEach((itemBulan) => {
        // Buat elemen <li>
        let listItem = document.createElement('li');
        listItem.onclick = function () {
            acttionPeriode(this);
        };
        listItem.id = 'filterKategori';
        listItem.setAttribute('data-id-2', '1');
        listItem.setAttribute('data-id', itemBulan.id_bulan);

        // Buat elemen <a> di dalam <li>
        let link = document.createElement('a');
        link.className = 'block py-2 px-4 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white';
        link.innerText = itemBulan.bulan_transaksi;

        // Sisipkan elemen <a> ke dalam elemen <li>
        listItem.appendChild(link);

        // Sisipkan elemen <li> ke dalam container
        contanerListItem.appendChild(listItem);
    });





}


