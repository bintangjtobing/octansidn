// const btnEditSup = Array.from(document.querySelectorAll('#btnEditSup'))
// const namaBisnis = document.getElementById('nama_bisnis_detail')
// const alamatBisnis = document.getElementById('alamat_detail')
// const emailBisnis = document.getElementById('email_detail')
// const noHp = document.getElementById('no_hp')
// const optTipe = Array.from(document.querySelectorAll('#optTipe'))
// const btnDeleteSupCos = document.getElementById('btnDeleteSup')
// const jnTransaksiSup = Array.from(document.querySelectorAll('#jnTransaksiSup'))

// btnEditSup.forEach(btnEditSup => {
//     btnEditSup.addEventListener('click', function() {
//         // console.log(btnDeleteSupCos);
//         fetch('get_sup_or_cos_by_no_hp/?id=' + this.getAttribute('data'))
//             .then(response => {
//                 if(!response.ok) {
//                     throw new Error('Network response was not ok');
//                 }

//                 return response.json()
//             })
//             .then(data => {
//                 data.forEach(item => {
//                     namaBisnis.value = item.nama_bisnis
//                     alamatBisnis.value = item.alamat
//                     emailBisnis.value = item.email
//                     noHp.value = item.no_hp
//                     btnDeleteSupCos.setAttribute('data-id', item.id)
//                     optTipe.forEach(itemOpt => {
//                         if(item.jenis_transaksi_id == 1 || item.jenis_transaksi_id == 2) {
//                             optTipe[0].selected = true
//                         }else if(item.jenis_transaksi_id == 3 || item.jenis_transaksi_id == 4){
//                             optTipe[1].selected = true
//                         }
//                     })
//                     jnTransaksiSup.forEach((itemJnTran, index) => {
//                         if(item.jenis_transaksi_id == itemJnTran.value){
//                             itemJnTran.selected = true
//                             console.log(itemJnTran.value);
//                         }
//                     })
//                 });

//             })
//             .catch(error => {
//                 console.error('fetch error', error)
//             })
//     })
// })


const barisSearch = document.getElementById('search-dropdown')
const tabelRow = Array.from(document.querySelectorAll('#tabel-row'))
const modalUpdate = document.getElementById('update_sup_cos')

const modalBackdrop = document.getElementById('modal-backdrop')
const closeModal = document.getElementById('close-modal')



closeModal.addEventListener('click', function(){
    modalBackdrop.classList.toggle('hidden')
    modalUpdate.classList.replace('flex', 'hidden')
})


barisSearch.addEventListener('keyup', function() {
    const xhr = new XMLHttpRequest()
    xhr.onload = function() {
        if(this.status === 200) {
            let response = JSON.parse(this.responseText)
            console.log(response);
            tabelRow.forEach(tr => {
                while(tr.firstChild) {
                    tr.removeChild(tr.firstChild)
                }
            })
            // rowTotalTransaksi.removeChild(rowTotalTransaksi.firstChild)
            if(response.length > 0) {
                response.forEach((res, index) => {

                    let element = `
                                    <th scope="row"
                                    class="px-4 py-3 font-medium text-gray-900 whitespace-nowrap dark:text-white">${res.nama_bisnis}</th>
                                    <td class="px-4 py-3">${res.tipe}</td>
                                    <td class="px-4 py-3">${res.jenis_transaksi}</td>
                                    <td class="px-4 py-3">${res.no_hp}</td>
                                <td class="px-4 py-3">${res.alamat}</td>
                                <td class="px-4 py-3">${res.email}</td>
                                <td class="px-4 py-3 flex items-center justify-end">
                                <button data-modal-toggle="update_sup_cos" data="${res.id}" id="btnEditSup">
                                    <svg class="w-6 h-6 text-gray-500 dark:text-white" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 20 18">
                                        <path d="M12.687 14.408a3.01 3.01 0 0 1-1.533.821l-3.566.713a3 3 0 0 1-3.53-3.53l.713-3.566a3.01 3.01 0 0 1 .821-1.533L10.905 2H2.167A2.169 2.169 0 0 0 0 4.167v11.666A2.169 2.169 0 0 0 2.167 18h11.666A2.169 2.169 0 0 0 16 15.833V11.1l-3.313 3.308Zm5.53-9.065.546-.546a2.518 2.518 0 0 0 0-3.56 2.576 2.576 0 0 0-3.559 0l-.547.547 3.56 3.56Z"/>
                                        <path d="M13.243 3.2 7.359 9.081a.5.5 0 0 0-.136.256L6.51 12.9a.5.5 0 0 0 .59.59l3.566-.713a.5.5 0 0 0 .255-.136L16.8 6.757 13.243 3.2Z"/>
                                    </svg>
                                </button>
                                </td>
                                `


                        tabelRow[index].innerHTML = element

                        const btnEditSup = Array.from(document.querySelectorAll('#btnEditSup'))
                        const namaBisnis = document.getElementById('nama_bisnis_detail')
                        const alamatBisnis = document.getElementById('alamat_detail')
                        const emailBisnis = document.getElementById('email_detail')
                        const noHp = document.getElementById('no_hp')
                        const optTipe = Array.from(document.querySelectorAll('#optTipe'))
                        const btnDeleteSupCos = document.getElementById('btnDeleteSup')
                        const jnTransaksiSup = Array.from(document.querySelectorAll('#jnTransaksiSup'))

                        const modalBackdrop = document.getElementById('modal-backdrop')
                        const closeModal = document.getElementById('close-modal')



                        closeModal.addEventListener('click', function(){
                            modalBackdrop.classList.toggle('hidden')
                            modalUpdate.classList.replace('flex', 'hidden')
                        })
                        btnEditSup.forEach(btnEditSup => {
                            btnEditSup.addEventListener('click', function() {
                                // console.log(btnDeleteSupCos);
                                modalUpdate.classList.replace('hidden', 'flex')
                                modalBackdrop.classList.toggle('hidden')
                                fetch('get_sup_or_cos_by_no_hp/?id=' + this.getAttribute('data'))
                                    .then(response => {
                                        if(!response.ok) {
                                            throw new Error('Network response was not ok');
                                        }

                                        return response.json()
                                    })
                                    .then(data => {
                                        console.log(data);
                                        data.forEach(item => {
                                            namaBisnis.value = item.nama_bisnis
                                            alamatBisnis.value = item.alamat
                                            emailBisnis.value = item.email
                                            noHp.value = item.no_hp
                                            btnDeleteSupCos.setAttribute('data-id', item.id)
                                            optTipe.forEach(itemOpt => {
                                                if(item.jenis_transaksi_id == 1 || item.jenis_transaksi_id == 2) {
                                                    optTipe[0].selected = true
                                                }else if(item.jenis_transaksi_id == 3 || item.jenis_transaksi_id == 4){
                                                    optTipe[1].selected = true
                                                }
                                            })
                                            jnTransaksiSup.forEach((itemJnTran, index) => {
                                                if(item.jenis_transaksi_id == itemJnTran.value){
                                                    itemJnTran.selected = true
                                                    console.log(itemJnTran.value);
                                                }
                                            })
                                        });

                                    })
                                    .catch(error => {
                                        console.error('fetch error', error)
                                    })
                            })
                        })
                })
            }else{
                tabelRow.forEach(tr => {
                    while(tr.firstChild) {
                        tr.removeChild(tr.firstChild)
                    }
                })
            }
        }
    }

    xhr.open('GET', '/search-by-nama-bisnis?nama_bisnis=' + this.value, true)
    xhr.send();
})
