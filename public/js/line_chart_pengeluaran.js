// ApexCharts options and config

window.addEventListener("load", function () {
    const lineChart = function (
        jumlahPendapatan,
        jumlahPengeluaran,
        dataTanggal,
        // dataKinerja
    ) {
        let options = {
            // set the labels option to true to show the labels on the X and Y axis
            xaxis: {
                show: true,
                categories: dataTanggal,
                labels: {
                    show: window.innerWidth >= 768 ? true : false,
                    style: {
                        fontFamily: "Inter, sans-serif",
                        cssClass:
                            "text-xs font-normal fill-gray-500 dark:fill-gray-400",
                        fontSize: "8px",
                    },
                },
                axisBorder: {
                    show: false,
                },
                axisTicks: {
                    show: false,
                },
            },
            yaxis: {
                show: true,
                labels: {
                    show: true,
                    style: {
                        fontFamily: "Inter, sans-serif",
                        cssClass:
                            "text-xs font-normal fill-gray-500 dark:fill-gray-400",
                    },
                    formatter: function (value) {
                        return value
                            .toLocaleString("id-ID", {
                                style: "currency",
                                currency: "IDR",
                            })
                            .split(",")[0];
                    },
                },
            },
            series: [
                {
                    name: "Pengeluaran Pokok",
                    data: jumlahPendapatan,
                    color: "#ff0000",
                },
                {
                    name: "Pengeluaran Tambahan",
                    data: jumlahPengeluaran,
                    color: "#EF6262",
                },
                // {
                //     name: "Kinerja Rata-Rata",
                //     data: dataKinerja,
                //     color: "#d24f98",
                // },
            ],
            chart: {
                sparkline: {
                    enabled: false,
                },
                height: "100%",
                width: "100%",
                type: "area",
                fontFamily: "Inter, sans-serif",
                dropShadow: {
                    enabled: false,
                },
                toolbar: {
                    show: false,
                },
            },
            tooltip: {
                enabled: true,
                x: {
                    show: false,
                },
            },
            fill: {
                type: "gradient",
                gradient: {
                    opacityFrom: 0.55,
                    opacityTo: 0,
                    shade: "#1C64F2",
                    gradientToColors: ["#1C64F2"],
                },
            },
            dataLabels: {
                enabled: false,
            },
            stroke: {
                curve: "smooth",
                width: 3,
            },
            legend: {
                show: true,
            },
            grid: {
                show: true,
            },
            markers: {
                size: 1,
            },
        };

        if (
            document.getElementById("labels-chart-2") &&
            typeof ApexCharts !== "undefined"
        ) {
            const chart = new ApexCharts(
                document.getElementById("labels-chart-2"),
                options
            );
            chart.render();
        }
    };

    const xhr = new XMLHttpRequest();

    xhr.onload = function () {
        if (this.status === 200) {
            let response = JSON.parse(this.responseText);
            const dataJumlahPendapatan = response.map((res) => res.jumlah);
            // const dataKinerja = response.map((res) => res.average);

            const xhr2 = new XMLHttpRequest();
            xhr2.onload = function () {
                if (this.status === 200) {
                    let response = JSON.parse(this.responseText);
                    const dataJumlahPengeluaran = response.map(
                        (res) => res.jumlah
                    );
                    const dataTanggal = response.map((res) => res.tanggal);
                    lineChart(
                        dataJumlahPendapatan,
                        dataJumlahPengeluaran,
                        dataTanggal,
                        // dataKinerja
                    );
                }
            };

            xhr2.open(
                "GET",
                "/get_transaksi_by_jenis_transaksi_id_line_chart?id=4"
            );
            xhr2.send();
        }
    };

    xhr.open(
        "GET",
        "/get_transaksi_by_jenis_transaksi_id_line_chart?id=3",
        true
    );
    xhr.send();
});
