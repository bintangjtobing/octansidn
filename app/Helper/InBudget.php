<?php

namespace App\Helper;

use App\Models\Anggaran;
use App\Models\Kategori_transaksi;
use App\Models\Transaksi;
use Illuminate\Support\Facades\DB;
use App\Models\Kategori_anggaran;
use App\Models\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\App;
use GuzzleHttp;
use Illuminate\Database\Console\Migrations\StatusCommand;
use Illuminate\Support\Facades\Auth;

use function PHPUnit\Framework\returnSelf;

class InBudget
{

    public static function konversiBudget()
    {
        // array pertama merupakan jumlah budgeting kebutuhan, kedua merupakan keinginan dan ketiga tabunagn
        $data = DatabaseHelper::getJumlahBudgeting();

        $jumlah = [];
        foreach($data as $item) {
            $jumlah[] = [$item->jumlah];
        }

        return $jumlah;
    }

    public static function pendapatanByKategori()
    {
       return Transaksi::join('kategori_transaksis', 'transaksis.kategori_transaksi_id', '=', 'kategori_transaksis.id')
                ->where('transaksis.user_id', auth()->user()->id)
                ->whereIn('transaksis.jenis_transaksi_id', [1, 2])
                ->whereIn('kategori_transaksis.jenis_transaksi_id', [1, 2])
                ->where('void', false)
                ->orderBy('tanggal', 'desc')
                ->with(['kategori_transaksi', 'jenis_transaksi', 'suppliers_or_customers'])
                ->get();
    }

    public static function pengeluaranByKategori()
    {
       return Transaksi::join('kategori_transaksis', 'transaksis.kategori_transaksi_id', '=', 'kategori_transaksis.id')
                ->where('transaksis.user_id', auth()->user()->id)
                ->whereIn('transaksis.jenis_transaksi_id', [3, 4])
                ->whereIn('kategori_transaksis.jenis_transaksi_id', [3, 4])
                ->where('void', false)
                ->orderBy('tanggal', 'desc')
                ->with(['kategori_transaksi', 'jenis_transaksi', 'suppliers_or_customers'])
                ->get();
    }


    // Kondisi cek apakah, pengeluaran berdasarkan jenis transaksi sesuai dengan budget anggaran atau bahkan melebihi budget anggaran
    public static function cekStatusTransaksiPengeluaran($parameter)
    {
        $data = DatabaseHelper::getPersentaseBudgeting();

        $result = 'transaksi pengeluaran pokok kosong';
        foreach($data as $item){
            if($item['nama'] == $parameter && $item['persentase'] < 100){
                $result = false;
            }else if($item['nama'] == $parameter && $item['persentase'] == 100){
                $result = true;
            }else if($item['nama'] == $parameter && $item['persentase'] > 100){
                $result = true;
            }
        }

        return $result;
    }

    public static function cekStatusTransaksiPengeluaranTambahan()
    {
        $data = DatabaseHelper::getPersentaseBudgeting();

        $result = 'transaksi pengeluaran tambahan kosong';
        foreach($data as $item){
            if($item['nama'] == 'keinginan' && $item['persentase'] < 100){
                $result = false;
            }else if($item['nama'] == 'keinginan' && $item['persentase'] == 100){
                $result = true;
            }else if($item['nama'] == 'keinginan' && $item['persentase'] > 100){
                $result = true;
            }
        }

        return $result;
    }

    // Kondisi cek apakah, pengeluaran berdasarkan jenis transaksi sesuai dengan budget anggaran atau bahkan melebihi budget anggaran end



    public static function getTopTransaksiByKategori()
    {
        return Transaksi::join('kategori_transaksis', 'transaksis.kategori_transaksi_id', '=', 'kategori_transaksis.id')
        ->select('kategori_transaksi_id', 'kategori_transaksis.nama',  'kategori_transaksis.jenis_transaksi_id',DB::raw('SUM(jumlah) as total_jumlah'))
        ->where('transaksis.user_id', auth()->user()->id)
        ->where('void', false)
        ->whereMonth('transaksis.tanggal', DatabaseHelper::getMonth())
        ->whereIn('transaksis.jenis_transaksi_id', [3, 4])
        ->groupBy('kategori_transaksi_id', 'kategori_transaksis.nama')
        ->orderByDesc('total_jumlah')
        ->limit(1)->get();
    }

    // 8
    public static function countMonth($array){
         // Mengatur zona waktu menjadi "Asia/Jakarta"
         $tanggalSaatIni = Carbon::now();

         // Mengatur zona waktu "Asia/Jakarta"
         $tanggalSaatIni->setTimezone('Asia/Jakarta');

         // Format tanggal sesuai dengan kebutuhan Anda
         $tanggalSaatIniFormatted = $tanggalSaatIni->format('Y-m-d H:i:s');

         // Mendapatkan bulan saat ini dalam bentuk nomor (1-12)
         $idMonth = $tanggalSaatIni->month;

         $data = Transaksi::where('user_id', auth()->user()->id)
                            ->where('void', 'false')
                            ->whereIn('jenis_transaksi_id', $array)
                            ->orderBy('created_at', 'desc')
                            ->whereMonth('tanggal', $idMonth)
                            ->count();


        return $data;
    }

    // 9
    public static function budgetingDIbagiCountMonth($array)
    {
        $data = DatabaseHelper::getJumlahBudgeting();


        foreach($data as $item) {
            if($item['nama'] == 'kebutuhan')
            $data = intval($item['jumlah']) / inBudget::countMonth($array);
        }

        return $data;
    }

    // 10
    public Static function rataRataJumlahKategoriTransaksi($array)
    {
        $data = Transaksi::join('kategori_transaksis', 'transaksis.kategori_transaksi_id', '=', 'kategori_transaksis.id')
                ->where('transaksis.user_id', auth()->user()->id)
                ->whereIn('transaksis.jenis_transaksi_id', $array)
                ->whereIn('kategori_transaksis.jenis_transaksi_id', $array)
                ->where('void', false)
                ->orderBy('tanggal', 'desc')
                ->with(['kategori_transaksi', 'jenis_transaksi', 'suppliers_or_customers'])
                ->sum('jumlah');

        return $data / InBudget::countMonth($array);
    }

    // 11
    public static function selisihKategoriTransaksiDenganAnggaranKebutuhan($array)
    {
        $data = Transaksi::join('kategori_transaksis', 'transaksis.kategori_transaksi_id', '=', 'kategori_transaksis.id')
                ->where('transaksis.user_id', auth()->user()->id)
                ->whereIn('transaksis.jenis_transaksi_id', $array)
                ->whereIn('kategori_transaksis.jenis_transaksi_id', $array)
                ->where('void', false)
                ->orderBy('tanggal', 'desc')
                ->with(['kategori_transaksi', 'jenis_transaksi', 'suppliers_or_customers'])
                ->sum('jumlah');

        $dataKategori = DatabaseHelper::getJumlahBudgeting();

        foreach($dataKategori as $item) {
            if($item['nama'] == 'kebutuhan') {
                $dataKategori = intval($item['jumlah']);
            }
        }

        return $dataKategori - $data;

    }

    // 12
    public static function rataRataKategoriTransaksiDibagiJumlahTanggal()
    {
        $dataKategoriJumlah = Transaksi::join('kategori_transaksis', 'transaksis.kategori_transaksi_id', '=', 'kategori_transaksis.id')
                                        ->select('kategori_transaksi_id', 'kategori_transaksis.nama',  'kategori_transaksis.jenis_transaksi_id',DB::raw('SUM(jumlah) as total_jumlah'))
                                        ->where('transaksis.user_id', auth()->user()->id)
                                        ->where('void', false)
                                        ->whereMonth('transaksis.tanggal', DatabaseHelper::getMonth())
                                        ->whereIn('transaksis.jenis_transaksi_id', [3, 4])
                                        ->groupBy('kategori_transaksi_id', 'kategori_transaksis.nama')
                                        ->get();

        $rataRataKategori = $dataKategoriJumlah->sum('total_jumlah') / $dataKategoriJumlah->count() / InBudget::countMonth([3]);

        return intval($rataRataKategori);
    }

    // 13
    public static function sumTotalJenisTransaksiYangMelebihiBudget($param, $array)
    {
        $dataKategoriJumlah = Transaksi::join('kategori_transaksis', 'transaksis.kategori_transaksi_id', '=', 'kategori_transaksis.id')
                            ->select('kategori_transaksi_id', 'kategori_transaksis.nama', 'kategori_transaksis.jenis_transaksi_id', DB::raw('SUM(jumlah) as total_jumlah'))
                            ->where('transaksis.user_id', auth()->user()->id)
                            ->where('void', false)
                            ->whereMonth('transaksis.tanggal', DatabaseHelper::getMonth())
                            ->whereIn('transaksis.jenis_transaksi_id', [3, 4])
                            ->where('kategori_transaksis.jenis_transaksi_id', '=', $array)
                            ->groupBy('kategori_transaksi_id', 'kategori_transaksis.nama')
                            ->get();

        $budgetingKebutuhan =  DatabaseHelper::getJumlahBudgeting();

        foreach($budgetingKebutuhan as $item) {
            if($item['nama'] == $param){
                $budgetingKebutuhan = intval($item['jumlah']);
            }
        }

        $jumlahSelisih = $dataKategoriJumlah->sum('total_jumlah') - $budgetingKebutuhan;

        return $jumlahSelisih;
    }

    // 14
    public static function sumSelisihTotalTransaksiBudgetingKebuthan($param, $array)
    {
        $data = Transaksi::join('kategori_transaksis', 'transaksis.kategori_transaksi_id', '=', 'kategori_transaksis.id')
        ->select('kategori_transaksi_id', 'kategori_transaksis.nama', 'kategori_transaksis.jenis_transaksi_id', DB::raw('SUM(jumlah) as total_jumlah'))
        ->where('transaksis.user_id', auth()->user()->id)
        ->where('void', false)
        ->whereMonth('transaksis.tanggal', DatabaseHelper::getMonth())
        ->whereIn('transaksis.jenis_transaksi_id', [3, 4])
        ->where('kategori_transaksis.jenis_transaksi_id', '=', $array)
        ->groupBy('kategori_transaksi_id', 'kategori_transaksis.nama')
        ->get();

        $budgetingKebutuhan =  DatabaseHelper::getJumlahBudgeting();

        foreach($budgetingKebutuhan as $item) {
            if($item['nama'] == $param){
                $budgetingKebutuhan = intval($item['jumlah']);
            }
        }

        $jumlahSelish = $budgetingKebutuhan - $data->sum('total_jumlah');

        return $jumlahSelish;
    }

    // 15

    public static function sumSelishTotalSelKategoriTransDgnBudgetingKebutuhan()
    {
        $budgetingKebutuhan =  DatabaseHelper::getJumlahBudgeting();

        foreach($budgetingKebutuhan as $item) {
            if($item['nama'] == 'kebutuhan'){
                $budgetingKebutuhan = intval($item['jumlah']);
            }
        }

        $data = Transaksi::join('kategori_transaksis', 'transaksis.kategori_transaksi_id', '=', 'kategori_transaksis.id')
        ->select('kategori_transaksi_id', 'kategori_transaksis.nama', 'kategori_transaksis.jenis_transaksi_id', DB::raw('SUM(jumlah) as total_jumlah'))
        ->where('transaksis.user_id', auth()->user()->id)
        ->where('void', false)
        ->whereMonth('transaksis.tanggal', DatabaseHelper::getMonth())
        ->whereIn('transaksis.jenis_transaksi_id', [3, 4])
        ->groupBy('kategori_transaksi_id', 'kategori_transaksis.nama')
        ->get();

        $jumlahSelKategori = $data->sum('total_jumlah');

        return $budgetingKebutuhan - $jumlahSelKategori;
    }

    // 16
    public static function rataRataSelisih($param, $array)
    {
        return InBudget::sumSelisihTotalTransaksiBudgetingKebuthan($param,$array) - InBudget::sumSelishTotalSelKategoriTransDgnBudgetingKebutuhan() / 2;
    }

    // 17
    public static function sumTotalKategoriTranDgnRataRataSelisih($param, $array)
    {
        $data = Transaksi::join('kategori_transaksis', 'transaksis.kategori_transaksi_id', '=', 'kategori_transaksis.id')
        ->select('kategori_transaksi_id', 'kategori_transaksis.nama', 'kategori_transaksis.jenis_transaksi_id', DB::raw('SUM(jumlah) as total_jumlah'))
        ->where('transaksis.user_id', auth()->user()->id)
        ->where('void', false)
        ->whereMonth('transaksis.tanggal', DatabaseHelper::getMonth())
        ->whereIn('transaksis.jenis_transaksi_id', [3, 4])
        ->groupBy('kategori_transaksi_id', 'kategori_transaksis.nama')
        ->get();

        $jumlahSelKategori = $data->sum('total_jumlah');

        return $jumlahSelKategori - InBudget::rataRataSelisih($param, $array);
    }

    public static function inBudget($param, $array)
    {

        if(inBudget::cekStatusTransaksiPengeluaran($param) == true){
            $no1 = InBudget::getTopTransaksiByKategori();
            $no2 = InBudget::budgetingDIbagiCountMonth($array);
            $no3 = InBudget::rataRataJumlahKategoriTransaksi($array);
            $no4 = InBudget::selisihKategoriTransaksiDenganAnggaranKebutuhan($array);
            $no5 = InBudget::rataRataKategoriTransaksiDibagiJumlahTanggal();
            $no6 = InBudget::sumTotalJenisTransaksiYangMelebihiBudget($param, $array);
            $no7 = InBudget::sumSelisihTotalTransaksiBudgetingKebuthan($param, $array);
            $no8 = InBudget::sumSelishTotalSelKategoriTransDgnBudgetingKebutuhan();
            $no9 = InBudget::rataRataSelisih($param, $array);
            $no10 = InBudget::sumTotalKategoriTranDgnRataRataSelisih($param, $array);

            return response()->json(['7' => $no1, '9' => $no2, '10' => $no3, '11' => $no4, '12' => $no5, '13' => $no6, '14' => $no7, '15' => $no8, '16' => $no9, '17' => $no10]);
        }


    }
}
