<?php

namespace App\Http\Middleware;

use App\Models\Pints;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class verify_pin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $pin = Pints::where('user_id', auth()->user()->id)->get();
        if(count($pin) === 0) {
            session()->put('no_pins', 'buat pin terlebih dahulu');
            // return redirect('/');
        };

        // if(!session()->has('sucses_verify_pin')){
        //     return redirect('/');
        // };

    return $next($request);
}
}
