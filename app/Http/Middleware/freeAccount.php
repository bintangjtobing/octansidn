<?php

namespace App\Http\Middleware;

use App\Models\Payment;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Models\User;

class freeAccount
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $user = User::where('id', auth()->user()->id)->with('payment')->first();
        $payment = Payment::where('user_id', auth()->user()->id)->orderBy('created_at', 'desc')->first() ?? '';


        if ($user->payment_id != null && $payment != '') {
            if($user->payment->status == 'pending' || $user->payment->status == 'failed'){
                session()->flash('pendingDashboard', 'Lanjutkan berlangganan');
            }else if($user->payment->status == 'expired'){
                session()->flash('expiredDashboard', 'berlangganan sekarang');
            }
        }else if($user->payment_id == null){
            session()->flash('freeDashboard', 'berlangganan sekarang');
        }

        if ($user->payment_id != null && $payment != '') {
            if($user->payment->status == 'pending' || $user->payment->status == 'failed'){
                session(['pending' => 'berlangganan sekarang']);
                session()->forget('free'); // Hapus sesi yang lama jika ada
            }else if($user->payment->status == 'expired'){
                session(['expired' => 'berlangganan sekarang']);
                session()->forget('free'); // Hapus sesi yang lama jika ada
                session()->forget('penidng'); // Hapus sesi yang lama jika ada
            }
        }else if($user->payment_id == null){
            session(['free' => 'berlangganan sekarang']);
        }
        return $next($request);
    }

}
