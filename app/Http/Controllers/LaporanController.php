<?php

namespace App\Http\Controllers;

use App\Exports\ExportLabaRugi;
use App\Exports\ExportPemasukan;
use App\Exports\ExportPengeluaran;
use App\Helper\DatabaseHelper;
use App\Models\Kategori_transaksi;
use App\Models\Laporan;
use App\Models\Transaksi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use LDAP\Result;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class LaporanController extends Controller
{
    /**
     * Display a listing of the resource.
     */

    public function showLaporanPemasukan()
    {

        return view('dashboard.laporan.pemasukan.index', [
            'pemasukan' => DatabaseHelper::getPendapatan(),
            'data' => Transaksi::join('kategori_transaksis', 'transaksis.kategori_transaksi_id', '=', 'kategori_transaksis.id')
                ->where('transaksis.user_id', auth()->user()->id)
                ->whereIn('transaksis.jenis_transaksi_id', [1, 2])
                ->whereIn('kategori_transaksis.jenis_transaksi_id', [1, 2])
                ->where('void', false)
                ->orderBy('tanggal', 'desc')
                ->with(['kategori_transaksi', 'jenis_transaksi', 'suppliers_or_customers'])
                ->paginate(15),
            'kategori' => Transaksi::join('kategori_transaksis', 'transaksis.kategori_transaksi_id', '=', 'kategori_transaksis.id')
                ->where('transaksis.user_id', auth()->user()->id)
                ->where('void', false)
                ->whereIn('transaksis.jenis_transaksi_id', [1, 2])
                ->distinct()
                ->select('kategori_transaksis.nama', 'kategori_transaksis.id')
                ->groupBy('kategori_transaksis.nama', 'kategori_transaksis.id')
                ->get(),
            'dataBulan' => DatabaseHelper::getMonthTransaki(),
            'dataTahun' => DatabaseHelper::getYearTransaki(),
        ]);
    }

    public function showLaporanPengeluaran()
    {
        return view('dashboard.laporan.pengeluaran.index', [
            'data' => Transaksi::join('kategori_transaksis', 'transaksis.kategori_transaksi_id', '=', 'kategori_transaksis.id')
                                ->where('transaksis.user_id', auth()->user()->id)
                                ->whereIn('transaksis.jenis_transaksi_id', [3,4])
                                ->whereIn('kategori_transaksis.jenis_transaksi_id', [3,4])
                                ->where('void', false)
                                ->orderBy('tanggal', 'desc')
                                ->with(['kategori_transaksi', 'jenis_transaksi', 'suppliers_or_customers'])
                                ->paginate(15),
            'pengeluaran' => DatabaseHelper::getPengeluaran(),
            'kategori' => Transaksi::join('kategori_transaksis', 'transaksis.kategori_transaksi_id', '=', 'kategori_transaksis.id')
                ->where('transaksis.user_id', auth()->user()->id)
                ->where('void', false)
                ->whereIn('transaksis.jenis_transaksi_id', [3, 4])
                ->distinct()
                ->select('kategori_transaksis.nama', 'kategori_transaksis.id')
                ->groupBy('kategori_transaksis.nama', 'kategori_transaksis.id')
                ->get(),
            'dataBulan' => DatabaseHelper::getMonthTransaki(),
            'user' => DatabaseHelper::getUser()[0],
            'dataTahun' => DatabaseHelper::getYearTransaki(),
        ]);
    }

    public function showLaporanLabaRugi()
    {
        $transaksi = Transaksi::where('user_id', auth()->user()->id)
            ->where('void', false);
        $namaBulan = null;


        $labaRugi = Transaksi::join('jenis_transaksis', 'transaksis.jenis_transaksi_id', '=', 'jenis_transaksis.id')
            ->where('transaksis.user_id', auth()->user()->id)
            ->where('transaksis.void', false)
            ->select(
                DB::raw('SUM(CASE WHEN jenis_transaksis.id IN (1,2) THEN transaksis.jumlah ELSE 0 END) - (SUM(CASE WHEN jenis_transaksis.id IN (3,4) THEN transaksis.jumlah ELSE 0 END) + SUM(CASE WHEN jenis_transaksis.id = 5 THEN transaksis.jumlah ELSE 0 END)) AS saldo')
            )
            ->get();

        if (request()->id != 'all') {
            $transaksi->whereMonth('tanggal', request()->id);
            $bulanAngka = request()->id;
            $tanggal = Carbon::create(null, $bulanAngka, 1);

            $namaBulan = $tanggal->isoFormat('MMMM');

            $labaRugi = Transaksi::join('jenis_transaksis', 'transaksis.jenis_transaksi_id', '=', 'jenis_transaksis.id')
            ->where('transaksis.user_id', auth()->user()->id)
            ->where('transaksis.void', false)
            ->whereMonth('tanggal', request()->id)
            ->select(
                DB::raw('SUM(CASE WHEN jenis_transaksis.id IN (1,2) THEN transaksis.jumlah ELSE 0 END) - (SUM(CASE WHEN jenis_transaksis.id IN (3,4) THEN transaksis.jumlah ELSE 0 END) + SUM(CASE WHEN jenis_transaksis.id = 5 THEN transaksis.jumlah ELSE 0 END)) AS saldo')
            )
            ->get();
        }

        $transaksi = $transaksi->get(); // Eksekusi query dan dapatkan hasilnya

        $pengeluaranSetelahPajak = $transaksi->whereIn('jenis_transaksi_id', [3, 4])->sum('jumlah') - $transaksi->whereIn('jenis_transaksi_id', [1,2])->sum('jumlah')/100*10 - $transaksi->whereIn('jenis_transaksi_id', [3,4])->sum('jumlah')/100*11;



        return view('dashboard.laporan.labarugi.index', [
            'pemasukan' => $transaksi->whereIn('jenis_transaksi_id', [1, 2])->sum('jumlah'),
            'pengeluaran' => $transaksi->whereIn('jenis_transaksi_id', [3, 4])->sum('jumlah'),
            'pemasukanByKategori' => DatabaseHelper::getTransaksiPemasukanGroupByKategori(),
            'pengeluaranByKategori' => DatabaseHelper::getTransaksiPengeluaranGroupByKategori(),
            'user' => DatabaseHelper::getUser()[0],
            'dataBulan' => DatabaseHelper::getMonthTransaki(),
            'bulan' => $namaBulan,
            'labaRugi' => $labaRugi,
            'pajakPemasukan' => $transaksi->whereIn('jenis_transaksi_id', [1,2])->sum('jumlah')/100*10,
            'pajakPengeluaran' => $transaksi->whereIn('jenis_transaksi_id', [3,4])->sum('jumlah')/100*11,
            'pengeluaranSetalahPajak' => $pengeluaranSetelahPajak,
            'dataTahun' => DatabaseHelper::getYearTransaki(),
        ]);
    }

    public function pemasukanExcel()
    {
        return Excel::download(new ExportPemasukan, 'laporan pemasukan.xlsx');
    }

    public function pengeluaranExcel()
    {
        return Excel::download(new ExportPengeluaran, 'laporan pengeluaran.xlsx');
    }

    public function labaRugiExcel()
    {
        return Excel::download(new ExportLabaRugi, 'Laporan Laba Rugi.xlsx');
    }

    public function getTransaksiByKategori()
    {
        $transaksi = Transaksi::with(['jenis_transaksi', 'kategori_transaksi', 'suppliers_or_customers'])
            ->where('user_id', auth()->user()->id)
            ->where('void', false)
            ->orderBy('tanggal', 'desc');

            $jenisTransaksiId = request()->jenis_transaksi_id;

            if ($jenisTransaksiId == 1) {
                $transaksi = $transaksi->whereIn('jenis_transaksi_id', [1, 2]);
            } elseif ($jenisTransaksiId == 3) {
                $transaksi = $transaksi->whereIn('jenis_transaksi_id', [3, 4]);
            } else if($jenisTransaksiId == 1 && request()->id == 'all') {
                $transaksi = $transaksi->whereIn('jenis_transaksi_id', [1, 2]);
            } else if($jenisTransaksiId == 1 && request()->id == 'all') {
                $transaksi = $transaksi->whereIn('jenis_transaksi_id', [3, 4]);
            }


        if (request()->id == 'all') {
            $transaksi = $transaksi;
        } else {
            $transaksi = $transaksi->where('kategori_transaksi_id', request()->id);
        }

        return [
            'data' => $transaksi->paginate(15),
            'id' => request()->id,
        ];
    }

    public function getTransaksiByMonth()
    {
        $transaksi = Transaksi::with(['jenis_transaksi', 'kategori_transaksi', 'suppliers_or_customers'])
            ->where('user_id', auth()->user()->id)
            ->where('void', false)
            ->orderBy('tanggal', 'desc');

        if (request()->id == 'all') {
            $transaksi = $transaksi->whereIn('jenis_transaksi_id', [1,2]);
        } else if(request()->jenis_transaksi_id == 1) {
            $transaksi->whereIn('jenis_transaksi_id', [1,2])->whereMonth('tanggal', request()->id);
        }else if(request()->jenis_transaksi_id == 3) {
            $transaksi->whereIn('jenis_transaksi_id', [3,4])->whereMonth('tanggal', request()->id);
        }


        return [
            'data' => $transaksi->paginate(15),
            'id' => request()->id,
            'jenis_transaksi_id' => request()->jenis_transaksi_id
        ];
    }

    public function getJumlahTransaksiByMonh()
    {
        $transaksi = Transaksi::with(['jenis_transaksi', 'kategori_transaksi', 'suppliers_or_customers'])
                            ->where('user_id', auth()->user()->id)
                            ->where('void', false);

        if (request()->id == 'all') {
            $transaksi = $transaksi->whereIn('jenis_transaksi_id', [1,2])->get();
        } else if(request()->jenis_transaksi_id == 1) {
            $transaksi->whereIn('jenis_transaksi_id', [1,2])->whereMonth('tanggal', request()->id)->get();
        }else if(request()->jenis_transaksi_id == 3) {
            $transaksi->whereIn('jenis_transaksi_id', [3,4])->whereMonth('tanggal', request()->id)->get();
        }

        return number_format($transaksi->sum('jumlah'), 0, ',', '.');

    }

    public function getJumlahTransaksiByKategori()
    {
        $transaksi = Transaksi::with(['jenis_transaksi', 'kategori_transaksi', 'suppliers_or_customers'])
        ->where('user_id', auth()->user()->id)
        ->where('void', false)
        ->orderBy('tanggal', 'desc');

    if (request()->id == 'all') {
        $transaksi = $transaksi->whereIn('jenis_transaksi_id', [1,2]);
    } else if(request()->jenis_transaksi_id == 1) {
        $transaksi->whereIn('jenis_transaksi_id', [1,2])->where('kategori_transaksi_id', request()->id);
    }else if(request()->jenis_transaksi_id == 3) {
        $transaksi->whereIn('jenis_transaksi_id', [3,4])->where('kategori_transaksi_id', request()->id);
    }


    return number_format($transaksi->sum('jumlah'), '0', ',', '.');

    }


    public function getKategoriTransaksiByYears()
    {
        $transaksi = Transaksi::with(['jenis_transaksi', 'kategori_transaksi', 'suppliers_or_customers'])
                            ->where('user_id', auth()->user()->id)
                            ->where('void', false);

        if (request()->id == 'all') {
            $transaksi = $transaksi->whereIn('jenis_transaksi_id', [1, 2]);
        } else if (request()->jenis_transaksi_id == 1) {
            $transaksi->whereIn('jenis_transaksi_id', [1, 2])->whereYear('tanggal', request()->id);;
        } else if (request()->jenis_transaksi_id == 3) {
            $transaksi->whereIn('jenis_transaksi_id', [3, 4])->whereYear('tanggal', request()->id);;
        }

        // Hapus get() dari blok if
        return [
            'data' => $transaksi->paginate(15),
            'id' => request()->id,
            'jenis_transaksi_id' => request()->jenis_transaksi_id
        ];

    }


    public function getJumlahKategoriTransaksiByYears()
    {
        $transaksi = Transaksi::with(['jenis_transaksi', 'kategori_transaksi', 'suppliers_or_customers'])
                            ->where('user_id', auth()->user()->id)
                            ->where('void', false);

        if (request()->id == 'all') {
            $transaksi = $transaksi->whereIn('jenis_transaksi_id', [1, 2]);
        } else if (request()->jenis_transaksi_id == 1) {
            $transaksi->whereIn('jenis_transaksi_id', [1, 2])->whereYear('tanggal', request()->id);;
        } else if (request()->jenis_transaksi_id == 3) {
            $transaksi->whereIn('jenis_transaksi_id', [3, 4])->whereYear('tanggal', request()->id);;
        }

        // Hapus get() dari blok if
        return number_format( $transaksi->sum('jumlah'), '0', '.' ,',');
    }

    public function getMonthsByYear()
    {
        return Transaksi::where('user_id', auth()->user()->id)
        ->where('void', false)
        ->whereYear('tanggal', request()->id)
        ->selectRaw('DATE_FORMAT(tanggal, "%M") as bulan_transaksi')
        ->selectRaw('DATE_FORMAT(tanggal, "%m") as id_bulan')
        ->distinct()
        ->get();
    }
}
