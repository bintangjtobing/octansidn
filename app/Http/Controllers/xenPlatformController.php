<?php

namespace App\Http\Controllers;

use App\Helper\DatabaseHelper;
use App\Models\informasiBisnis;
use App\Models\Octans_platform;
use Illuminate\Http\Request;
use Xendit\Configuration;
use Xendit\BalanceAndTransaction\BalanceApi;



class xenPlatformController extends Controller
{

    public function index()
    {
        return view('dashboard.admin.octansPlatform.index', [
            'data' => informasiBisnis::with('user','octans_platform')->paginate(15)
        ]);
    }

    public function createAccount()
    {
        // Set API Key dan Secret Key
        $xenditKey = env('API_KEY_XENDIT');
        $xenditSecret = '';

        // Buat header
        $headers = [
            'Content-Type: application/json',
            'Authorization: Basic ' . base64_encode($xenditKey . ':' . $xenditSecret),
        ];

        // $dataBisnis = informasiBisnis::where('user_id', auth()->user()->id)->first();

        // Buat data JSON untuk permintaan
        $data = [
            'email' => 'bahari49@gmail.com',
            'type' => 'MANAGED',
            'public_profile' => [
                'business_name' => 'PT ABC'
            ],
        ];

        // return $data;

        // Buat permintaan POST
        $ch = curl_init('https://api.xendit.co/v2/accounts');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Kirim permintaan
        $response = curl_exec($ch);

        // Tutup koneksi
        curl_close($ch);

        // Jika permintaan berhasil
        if ($response) {
            // Dekode JSON response
            $result = json_decode($response, true);

            // Jika status berhasil
            if (isset($result['status']) && $result['status'] === 'INVITED') {
                // Akun berhasil dibuat
                $octans = new Octans_platform();
                $octans->type = strtolower($result['type']);
                $octans->status = strtolower($result['status']);
                $octans->external_id = $result['id'];
                $octans->user_id = auth()->user()->id;
                if($octans->save()){
                    return redirect('/profile')->with('suksesCreateAccount', 'Octans platform memungkinkan para pengguna/pelaku usaha mengelola semua transaksi
                    pembayaran mereka');
                }
            } else {
                return redirect('/profile')->with('errorCreateAccount', 'Gagal meaktivasi Octans platform, hubungi kami jika permasalahan berlanjut');
            }
        } else {
            // Permintaan gagal
            return null;
        }
    }

    public function getBalance()
    {
        Configuration::setXenditKey(env('API_KEY_XENDIT'));

        $apiInstance = new BalanceApi();
        $account_type = "CASH"; // string | The selected balance type
        $currency = "IDR"; // string | Currency for filter for customers with multi currency accounts
        $for_user_id = DatabaseHelper::externalIdOctansPlatform(); // string | The sub-account user-id that you want to make this transaction for. This header is only used if you have access to xenPlatform. See xenPlatform for more information

        try {
            $result = $apiInstance->getBalance($account_type, $currency, $for_user_id);
            return $result['balance'];
        } catch (\Xendit\XenditSdkException $e) {
            echo 'Exception when calling BalanceApi->getBalance: ', $e->getMessage(), PHP_EOL;
            echo 'Full Error: ', json_encode($e->getFullError()), PHP_EOL;
        }
    }

    public function getTransaksiByUserId()
    {

        $xenditKey = env('API_KEY_XENDIT');
        $xenditSecret = '';

        // Buat header
        $headers = [
            'Content-Type: application/json',
            'Authorization: Basic ' . base64_encode($xenditKey . ':' . $xenditSecret),
            'for_user_id : 65965d4d4ebe9a69aedf95d0'
        ];

        // Buat objek cURL
        $ch = curl_init('https://api.xendit.co/transactions');

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        // Set opsi cURL
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Set parameter
        $url = 'https://api.xendit.co/transactions/';
        curl_setopt($ch, CURLOPT_URL, $url);

        // Kirim permintaan
        $response = curl_exec($ch);

        // Tutup koneksi
        curl_close($ch);

        return $response;

        // Jika permintaan berhasil
        if ($response) {
            // Dekode JSON response
            $result = json_decode($response, true);

            // Cetak hasil
            return $result;
        } else {
            // Permintaan gagal
            echo 'Permintaan gagal';
        }

    }


    public function makeQrCode()
    {
        $xenditKey = env('API_KEY_XENDIT');
        $xenditSecret = '';

        $account = Octans_platform::where('user_id', auth()->user()->id)->first();

        // Buat header
        $headers = [
            'Content-Type: application/json',
            'Authorization: Basic ' . base64_encode($xenditKey . ':' . $xenditSecret),
            'for_user_id:' . $account->external_id,
            'api-version: 2022-07-31',
        ];

        $payloads = [
            'reference_id' => 'test_qr_code',
            'type' => 'DYNAMIC',
            'amount' => 20000,
            'currency' => 'IDR'
        ];

        // Buat objek cURL
        $ch = curl_init('https://api.xendit.co/qr_codes');

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        // Set opsi cURL
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Set parameter
        $url = 'https://api.xendit.co/qr_codes/';
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($payloads));

        // Kirim permintaan
        $response = curl_exec($ch);

        // Tutup koneksi
        curl_close($ch);

        return $response;
    }



    public function createAccountByAdmin()
    {
        // Set API Key dan Secret Key
        $xenditKey = env('API_KEY_XENDIT');
        $xenditSecret = '';

        // Buat header
        $headers = [
            'Content-Type: application/json',
            'Authorization: Basic ' . base64_encode($xenditKey . ':' . $xenditSecret),
        ];

        // $dataBisnis = informasiBisnis::where('user_id', auth()->user()->id)->first();

        // Buat data JSON untuk permintaan
        $data = [
            'email' => request()->email,
            'type' => 'OWNED',
            'public_profile' => [
                'business_name' => request()->nama_bisnis
            ],
        ];

        // return $data;

        // Buat permintaan POST
        $ch = curl_init('https://api.xendit.co/v2/accounts');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Kirim permintaan
        $response = curl_exec($ch);

        // Tutup koneksi
        curl_close($ch);

        // Jika permintaan berhasil
        if ($response) {
            // Dekode JSON response
            $result = json_decode($response, true);

            // Jika status berhasil
            if (isset($result['status']) && $result['status'] === 'INVITED' || $result['status'] == 'REGISTERED') {
                // Akun berhasil dibuat
                $octans = new Octans_platform();
                $octans->type = strtolower($result['type']);
                $octans->status = strtolower($result['status']);
                $octans->external_id = $result['id'];
                $octans->user_id = auth()->user()->id;


                if($octans->save()){
                    $octansPlatform = Octans_platform::where('external_id', $result['id'])->value('id');
                    $infoBisnis = informasiBisnis::where('user_id', auth()->user()->id)->update([
                        'octans_platform_id' => $octansPlatform,
                    ]);
                    if($infoBisnis) {
                        return redirect('/octans-platform')->with('suksesAddAccount', 'Octans platform memungkinkan para pengguna/pelaku usaha mengelola semua transaksi
                        pembayaran mereka');
                    }
                }
            } else {
                return redirect('/profile')->with('errorCreateAccount', 'Gagal meaktivasi Octans platform, hubungi kami jika permasalahan berlanjut');
            }
        } else {
            // Permintaan gagal
            return null;
        }
    }


}
