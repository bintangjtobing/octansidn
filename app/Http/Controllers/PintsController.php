<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePintsRequest;
use App\Http\Requests\UpdatePintsRequest;
use App\Models\Pints;
use Clockwork\Request\Request;

class PintsController extends Controller
{
    public function storePin(Request $request)
    {
        $validated = request()->validate([
            'pin' => ['required', 'numeric', 'digits:6'],
            'confirm_pin' => ['required', 'same:pin'],
            'user_id' => ['unique:pins,user_id'], // Menambahkan aturan unique:pints,user_id
        ], [
            'pin.required' => 'PIN wajib diisi.',
            'pin.digits' => 'PIN harus terdiri dari 6 digit.',
            'pin.numeric' => 'PIN harus berupa angka.',
            'confirm_pin.required' => 'Konfirmasi PIN wajib diisi.',
            'confirm_pin.same' => 'Konfirmasi PIN harus sama dengan PIN.',
            'user_id.unique' => 'User ID sudah digunakan untuk PIN lain.', // Pesan untuk validasi unique:user_id
        ]);

        $pints = Pints::create([
            'pin' => $validated['pin'],
            'user_id' => $validated['user_id']
        ]);

        if($pints){
            session()->put('sucses_verify_pin', 'verifikasi pin berhasil');
            session()->forget('verify-pin');
            return redirect('/');
        }
    }


    public function verifyPin()
    {
        $validated = request()->validate([
            'pin' => ['required'],
        ]);

        $pints = Pints::where('user_id', auth()->user()->id)->where('pin', $validated['pin'])->get();

        if(count($pints) > 0) {
            session()->put('sucses_verify_pin', 'verifikasi pin berhasil');
            return redirect('/');
        }else{
            return redirect('/')->with('faild_verify_pin', 'gagal verifikasi pin');
        }
    }
}
