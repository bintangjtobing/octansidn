<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use Illuminate\Http\Request;

class billingController extends Controller
{
    public function index()
    {

        $dataBulan = Payment::where('user_id', auth()->user()->id)
        ->where('status', 'successful')
        ->selectRaw('DATE_FORMAT(created_at, "%M") as bulan_transaksi')
        ->selectRaw('DATE_FORMAT(created_at, "%m") as id_bulan')
        ->distinct()
        ->get();

        return view('dashboard.billing.index', [
            'data' => Payment::where('user_id', auth()->user()->id)->get(),
            'bulan' => $dataBulan,
        ]);
    }
}
