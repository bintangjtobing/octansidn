<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Pints extends Model
{
    use HasFactory;
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

     protected $table = 'pins';

    protected $fillable = [
        'user_id',
        'pin',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'pin',
    ];

    /**
     * Get the user that owns the pin.
     */
    // public function user()
    // {
    //     return $this->belongsTo(User::class);
    // }

    /**
     * Verify if the given PIN matches the stored PIN.
     *
     * @param string $pin
     * @return bool
     */
    // public function verifyPin(string $pin): bool
    // {
    //     return password_verify($pin, $this->pin); // Assumes bcrypt hashing for PIN
    // }
}
