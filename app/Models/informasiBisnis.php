<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use App\Models\User;
use Illuminate\Database\Eloquent\Relations\HasOne;
use App\Models\Octans_platform;

class informasiBisnis extends Model
{
    use HasFactory;

    protected $fillable = [
        'nama_bisnis',
        'alamat',
        'no_tax',
        'website',
        'email',
        'logo',
        'no_handphone',
        'jabatan',
        'user_id'
    ];


    public function user() : BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function octans_platform() : BelongsTo
    {
        return $this->BelongsTo(Octans_platform::class);
    }

}
