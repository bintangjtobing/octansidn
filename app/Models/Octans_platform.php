<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Octans_platform extends Model
{
    use HasFactory;
    protected $table = 'octans_platform';
}
