@extends('user.layouts.main_layouts')


@section('container')
    <style>
        .background {
            background-image: url('https://res.cloudinary.com/du0tz73ma/image/upload/w_1000/q_auto:best/f_auto/v1703068124/octansidn-login_cghluk.svg');
            background-size: cover;
            background-repeat: no-repeat
        }
    </style>
    <section class="dark:bg-gray-900 background" style="background-color:#F6F2F4;">

        <div class="flex flex-col items-center justify-center px-6 py-8 mx-auto h-screen md:h-screen lg:py-0">
            <a href="#" class="flex items-center mb-6 text-2xl font-semibold text-gray-900 dark:text-white">
                <img class="h-16 mr-2" src="../image/logo/octansidnByBoxity.png" alt="OctansIDN Logo Official">
            </a>
            <div
                class="w-full p-6 bg-white rounded-lg shadow dark:border md:mt-0 sm:max-w-md dark:bg-gray-800 dark:border-gray-700 sm:p-8">
                <h2 class="mb-1 text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-2xl dark:text-white">
                    Ubah kata sandi
                </h2>
                @if (session('status'))
                    <div class="flex items-center p-4 mb-4 text-sm text-blue-800 border border-blue-300 rounded-lg bg-blue-50 dark:bg-gray-800 dark:text-blue-400 dark:border-blue-800"
                        role="alert">
                        <svg class="flex-shrink-0 inline w-4 h-4 me-3" aria-hidden="true" xmlns="http://www.w3.org/2000/svg"
                            fill="currentColor" viewBox="0 0 20 20">
                            <path
                                d="M10 .5a9.5 9.5 0 1 0 9.5 9.5A9.51 9.51 0 0 0 10 .5ZM9.5 4a1.5 1.5 0 1 1 0 3 1.5 1.5 0 0 1 0-3ZM12 15H8a1 1 0 0 1 0-2h1v-3H8a1 1 0 0 1 0-2h2a1 1 0 0 1 1 1v4h1a1 1 0 0 1 0 2Z" />
                        </svg>
                        <span class="sr-only">Info</span>
                        <div>
                            <span class="font-medium">Info!</span> {{ session('status') }}
                        </div>
                    </div>
                @endif

                <form class="mt-4 space-y-4 lg:mt-5 md:space-y-5" action="/forgot-password" method="POST">
                    @csrf
                    <div>
                        <label for="email"
                            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Email</label>
                        <input type="email" name="email" id="email"
                            class="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-[#9345a3] focus:border-[#9345a3] block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-[#9345a3] dark:focus:border-[#9345a3]"
                            placeholder="name@company.com" required="">
                    </div>
                    <button type="submit"
                        class="primary-color w-full text-white focus:ring-4 focus:outline-none focus:ring-[#d24f98] font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:focus:ring-[#d24f98]">Kirim
                        link ubah kata sandi</button>
                </form>
            </div>
            <p class="text-sm text-gray-700 my-5">© Copyright <?php $Y = date('Y');
            echo $Y; ?> - OctansIDN | Reserved By <abbr
                    title="PT Boxity Central Indonesia">BoxityID</abbr></p>
        </div>
    </section>
@endsection
