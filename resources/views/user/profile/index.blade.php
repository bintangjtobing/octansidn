@extends('dashboard.layouts.main')


@section('container')
@include('user.profile.layouts.modalUplodImage')
{{-- @include('user.profile.layouts.modalUplodLogo') --}}
@include('user.profile.layouts.deleteImageModal')
@include('user.profile.layouts.confirm_aktif_octans')

<section class="mb-32 lg:mb-20">

    @if (session()->has('suksesCreateAccount'))
    <div id="successModal" tabindex="-1" aria-hidden="true"
        class="flex overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0 z-50 justify-center items-center w-full md:inset-0 h-modal md:h-full">
        <div class="relative p-4 w-full max-w-md h-full md:h-auto">
            <!-- Modal content -->
            <div class="relative p-4 text-center bg-white rounded-lg shadow dark:bg-gray-800 sm:p-5">
                <button onclick="modalHidden()" type="button"
                    class="text-gray-400 absolute top-2.5 right-2.5 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-600 dark:hover:text-white">
                    <svg aria-hidden="true" class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20"
                        xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                            d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                            clip-rule="evenodd"></path>
                    </svg>
                    <span class="sr-only">Close modal</span>
                </button>
                <div
                    class="w-12 h-12 rounded-full bg-green-100 dark:bg-green-900 p-2 flex items-center justify-center mx-auto mb-3.5">
                    <svg aria-hidden="true" class="w-8 h-8 text-green-500 dark:text-green-400" fill="currentColor"
                        viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                            d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z"
                            clip-rule="evenodd"></path>
                    </svg>
                    <span class="sr-only">Success</span>
                </div>
                <p class="mb-4 font-semibold text-gray-900 dark:text-white">Akun berhasil dibuat, aktifasi dilakukan
                    setelah 3-5 hari setelah pengiriman dokumen</p>
                <button onclick="modalHidden()" type="button"
                    class="py-2 px-3 text-sm font-medium text-center text-white rounded-lg primary-color">
                    Continue
                </button>
            </div>
        </div>
    </div>
    <div id="modal-backdrop" modal-backdrop="" class="bg-gray-900 bg-opacity-50 dark:bg-opacity-80 fixed inset-0 z-40">
    </div>
    @endif

    @if (session()->has('errorCreateAccount'))
    <!-- Main modal -->
    <div id="modalError" tabindex="-1" aria-hidden="true"
        class="flex overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0 z-50 justify-center items-center w-full md:inset-0 h-modal md:h-full">
        <div class="relative p-4 w-full max-w-md h-full md:h-auto">
            <!-- Modal content -->
            <div class="relative p-4 text-center bg-white rounded-lg shadow dark:bg-gray-800 sm:p-5">
                <button onclick="modalErrorHidden()" type="button"
                    class="text-gray-400 absolute top-2.5 right-2.5 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-600 dark:hover:text-white"
                    data-modal-toggle="">
                    <svg aria-hidden="true" class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20"
                        xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                            d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                            clip-rule="evenodd"></path>
                    </svg>
                    <span class="sr-only">Close modal</span>
                </button>
                <svg class="w-9 h-9 text-red-600 mx-auto mb-4" aria-hidden="true" xmlns="http://www.w3.org/2000/svg"
                    fill="none" viewBox="0 0 20 20">
                    <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                        d="M8 9h2v5m-2 0h4M9.408 5.5h.01M19 10a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z" />
                </svg>
                <p class="mb-4 text-gray-500 text-sm dark:text-gray-300">{{session('errorCreateAccount')}}</p>
                <div class="flex justify-center items-center space-x-4">
                    <button onclick="modalErrorHidden()" data-modal-toggle="" type="button"
                        class="py-2 px-3 text-sm font-medium text-gray-500 bg-white rounded-lg border border-gray-200 hover:bg-gray-100 focus:ring-4 focus:outline-none focus:ring-primary-300 hover:text-gray-900 focus:z-10 dark:bg-gray-700 dark:text-gray-300 dark:border-gray-500 dark:hover:text-white dark:hover:bg-gray-600 dark:focus:ring-gray-600">
                        Kembali
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-backdrop-error" modal-backdrop=""
        class="bg-gray-900 bg-opacity-50 dark:bg-opacity-80 fixed inset-0 z-40"></div>


    @endif

    {{-- info aktifasi --}}

    <div id="infoAktifasiModal" tabindex="-1" aria-hidden="true" class="hidden overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0 z-50 justify-center items-center w-full md:inset-0 h-modal md:h-full">
        <div class="relative p-4 w-full max-w-md h-full md:h-auto">
            <!-- Modal content -->
            <div class="relative p-4 text-center bg-white rounded-lg shadow dark:bg-gray-800 sm:p-5">
                <button type="button" class="text-gray-400 absolute top-2.5 right-2.5 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-600 dark:hover:text-white" data-modal-toggle="infoAktifasiModal">
                    <svg aria-hidden="true" class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
                    <span class="sr-only">Close modal</span>
                </button>
                <svg class="w-9 h-9 text-gray-600 mx-auto mb-4 dark:text-white" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 20">
                    <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 9h2v5m-2 0h4M9.408 5.5h.01M19 10a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"/>
                  </svg>
                <p class="mb-4 text-gray-500 text-sm dark:text-gray-300">Octans platform anda sedang di aktifasi, terus cek secara berkala untuk menegetahui proses aktifasi</p>
                <div class="flex justify-center items-center space-x-4">
                    <button data-modal-toggle="infoAktifasiModal" type="button" class="py-2 px-3 text-sm font-medium text-gray-500 bg-white rounded-lg border border-gray-200 hover:bg-gray-100 focus:ring-4 focus:outline-none focus:ring-primary-300 hover:text-gray-900 focus:z-10 dark:bg-gray-700 dark:text-gray-300 dark:border-gray-500 dark:hover:text-white dark:hover:bg-gray-600 dark:focus:ring-gray-600">
                        Kembali
                    </button>
                </div>
            </div>
        </div>
    </div>

    {{-- info aktifasi end --}}

    <h2 class="text-2xl font-semibold my-5">Profile user</h2>
    <div class="grid grid-cols-1 lg:grid-cols-2 gap-4">
        <div class="col-span-1">

            {{-- Profile Picture --}}

            <div
                class="bg-white rounded-md border p-5 shadow-sm border border-gray-200 flex flex-col xl:flex-row gap-4">
                <div>
                    <img class="w-28 h-28 rounded"
                        src="{{ $user->foto ?? 'https://res.cloudinary.com/du0tz73ma/image/upload/w_1000/q_auto:best/f_auto/v1700278579/default-profile_y2huqf.jpg' }}"
                        alt="foto user - {{ isset($user->username) }}">
                </div>
                <div>
                    <h3 class="text-xl font-semibold">Foto profile</h3>
                    <p class="text-gray-500 text-sm">JPG, GIF atau PNG, maksimal 800K</p>
                    <div class="mt-4 flex items-center gap-3">
                        <button type="button" data-modal-target="default-modal" data-modal-toggle="default-modal"
                            class="primary-color py-2 font-medium px-3 rounded-md text-sm text-white flex gap-2">
                            <svg class="w-5 h-5 dark:text-white" aria-hidden="true" xmlns="http://www.w3.org/2000/svg"
                                fill="none" viewBox="0 0 16 20">
                                <path fill="currentColor"
                                    d="M11.045 7.514a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0Zm-4.572 3.072L3.857 15.92h7.949l-1.811-3.37-1.61 2.716-1.912-4.679Z" />
                                <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"
                                    stroke-width="2"
                                    d="M6 1v4a1 1 0 0 1-1 1H1m14 12a.97.97 0 0 1-.933 1H1.933A.97.97 0 0 1 1 18V5.828a2 2 0 0 1 .586-1.414l2.828-2.828A2 2 0 0 1 5.828 1h8.239A.97.97 0 0 1 15 2v16ZM11.045 7.514a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0ZM3.857 15.92l2.616-5.333 1.912 4.68 1.61-2.717 1.81 3.37H3.858Z" />
                            </svg>{{ isset($user->foto) ? 'Ubah' : 'Unggah' }}
                            foto
                        </button>
                        <button id="deleteButton" data-modal-toggle="deleteModal" type="button"
                            data-foto="{{ $user->foto }}"
                            class="py-2 px-3 {{ isset($user->foto) ? '' : 'hidden' }} font-medium text-sm text-gray-600 rounded-md border flex items-center gap-2">
                            <svg class="w-4 h-4 dark:text-white" aria-hidden="true" xmlns="http://www.w3.org/2000/svg"
                                fill="none" viewBox="0 0 18 20">
                                <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"
                                    stroke-width="2"
                                    d="M1 5h16M7 8v8m4-8v8M7 1h4a1 1 0 0 1 1 1v3H6V2a1 1 0 0 1 1-1ZM3 5h12v13a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V5Z" />
                            </svg>
                            Hapus
                        </button>
                    </div>
                </div>
            </div>

            {{-- Profile Picture end --}}


            {{-- Informasi User --}}

            <div class="bg-white rounded-md border shadow-sm border border-gray-200 p-5 my-5">
                <h3 class="text-xl font-semibold mb-3">Informasi Pengguna</h3>

                <form method="post" action="/user">
                    @method('PUT')
                    @csrf
                    <div class="grid gap-6 mb-6 md:grid-cols-2">
                        <div>
                            <label for="nama"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Nama</label>
                            <input type="text" id="nama" name="nama" value="{{$user->nama}}"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="Nama kamu" required>
                        </div>
                        <div>
                            <label for="username"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Username</label>
                            <input type="text" id="username" name="username" value="{{$user->username}}"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="Username kamu" required>
                        </div>
                        <div>
                            <label for="email"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Email</label>
                            <input type="email" value="{{$user->email}}" name="email" id="email"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="Email kamu" required>
                        </div>
                        <div>
                            <label for="alamat"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Alamat</label>
                            <input type="text" id="alamat" name="alamat" value="{{$user->alamat}}"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="Alamat kamu" required>
                        </div>
                        <div class="md:col-span-2">
                            <label for="no_handphone"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Nomor
                                handphone</label>
                            <input type="tel" id="no_handphone" value="{{$user->no_handphone}}" name="no_handphone"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="No handphone kamu" required>
                        </div>
                    </div>
                    <button
                        class="py-2 px-3 rounded-md text-white flex items-center gap-2 primary-color text-sm font-medium">
                        <svg class="w-4 h-4 dark:text-white" aria-hidden="true" xmlns="http://www.w3.org/2000/svg"
                            fill="none" viewBox="0 0 10 14">
                            <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M5 13V1m0 0L1 5m4-4 4 4" />
                        </svg>
                        Simpan
                    </button>
                </form>

            </div>

            {{-- Informasi User End --}}
        </div>


        {{-- Informasi Bisnis --}}


        {{-- modal delete logo --}}
        <div id="deleteLogoModal" tabindex="-1" aria-hidden="true"
            class="hidden overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0 z-50 justify-center items-center w-full md:inset-0 h-modal md:h-full">
            <div class="relative p-4 w-full max-w-md h-full md:h-auto">
                <!-- Modal content -->
                <div class="relative p-4 text-center bg-white rounded-lg shadow dark:bg-gray-800 sm:p-5">
                    <button type="button"
                        class="text-gray-400 absolute top-2.5 right-2.5 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-600 dark:hover:text-white"
                        data-modal-toggle="deleteLogoModal">
                        <svg aria-hidden="true" class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20"
                            xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd"
                                d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                                clip-rule="evenodd"></path>
                        </svg>
                        <span class="sr-only">Close modal</span>
                    </button>
                    <svg class="text-gray-400 dark:text-gray-500 w-11 h-11 mb-3.5 mx-auto" aria-hidden="true"
                        fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                            d="M9 2a1 1 0 00-.894.553L7.382 4H4a1 1 0 000 2v10a2 2 0 002 2h8a2 2 0 002-2V6a1 1 0 100-2h-3.382l-.724-1.447A1 1 0 0011 2H9zM7 8a1 1 0 012 0v6a1 1 0 11-2 0V8zm5-1a1 1 0 00-1 1v6a1 1 0 102 0V8a1 1 0 00-1-1z"
                            clip-rule="evenodd"></path>
                    </svg>
                    <p class="mb-4 text-gray-500 dark:text-gray-300">Are you sure you want to delete this item?</p>
                    <div class="flex justify-center items-center gap-4">
                        <form action="">
                            <button data-modal-toggle="deleteLogoModal" type="button"
                                class="py-2 px-3 text-sm font-medium text-gray-500 bg-white rounded-lg  hover:bg-gray-100 focus:ring-4 focus:outline-none focus:ring-[#9345a3] hover:text-gray-900 focus:z-10 dark:bg-gray-700 dark:text-gray-300 dark:border-gray-500 dark:hover:text-white dark:hover:bg-gray-600 dark:focus:ring-gray-600">
                                No, cancel
                            </button>
                        </form>
                        <form id="" method="POST" action="/delete_logo">
                            @csrf
                            @method('DELETE')
                            <input type="hidden" name="logo"
                                value="{{ isset($info_bisnis[0]) ? $info_bisnis[0]->logo : '' }}">
                            <button type="submit"
                                class="py-2 px-3 text-sm font-medium text-center text-white bg-red-600 rounded-lg hover:bg-red-700 focus:ring-4 focus:outline-none focus:ring-red-300 dark:bg-red-500 dark:hover:bg-red-600 dark:focus:ring-red-900">
                                Yes, I'm sure
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        {{-- Modal delete logo end --}}


        <div class="col-span-1">
            <div
                class="bg-white rounded-md border p-5 shadow-sm border border-gray-200 flex flex-col xl:flex-row gap-4">
                <div class="flex items-center justify-center">
                    <div for="logoImage"
                        class="flex flex-col items-center justify-center rounded-lg bg-gray-50 dark:hover:bg-bray-800">
                        <div class="flex flex-col items-center justify-center">
                            <div class="relative border rounded">
                                <img id="avatar" class="w-28 h-28 rounded {{(isset($info_bisnis[0]->logo) ? '' : '')}}"
                                    src="{{ isset($info_bisnis[0]->logo) ? $info_bisnis[0]->logo : 'https://res.cloudinary.com/du0tz73ma/image/upload/w_1000/q_auto:best/f_auto/v1700279273/building_z7thy7.png' }}"
                                    alt="Logo bisnis kamu">
                                {{-- <div class="w-28 h-28 text-dark absolute flex items-center justify-center {{(isset($info_bisnis[0]->logo) ? 'hidden' : 'opacity-30')}}
                                hover:opacity-50 dark:text-white top-0">
                                <svg class="w-8 h-8 text-dark-500 flex items-center justify-center" aria-hidden="true"
                                    xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 16">
                                    <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"
                                        stroke-width="2"
                                        d="M13 13h3a3 3 0 0 0 0-6h-.025A5.56 5.56 0 0 0 16 6.5 5.5 5.5 0 0 0 5.207 5.021C5.137 5.017 5.071 5 5 5a4 4 0 0 0 0 8h2.167M10 15V6m0 0L8 8m2-2 2 2" />
                                </svg>
                            </div> --}}
                        </div>
                    </div>
                    {{-- <input onchange="previewImageLogo()" id="logoImage" name="logo" type="file" class="hidden" /> --}}
                </div>
            </div>
            <div>
                <h3 class="text-xl font-semibold">Logo bisnis</h3>
                <p class="text-gray-500 text-sm">JPG, GIF atau PNG, maksimal 800K</p>
                <div class="mt-4 flex items-center gap-3">
                    <form action="/store-logo" class="flex gap-3 items-center" method="POST"
                        enctype="multipart/form-data">
                        @csrf
                        <input type="file" class="hidden" name="logo" id="inputLogo">

                        <label id="labelInput"
                            class="cursor-pointer {{(isset($info_bisnis[0]->logo) ? 'primary-color font-medium text-white' : 'border border-gray-500')}} py-2 px-3 rounded-md text-sm flex gap-2"
                            for="logoImage">
                            <svg class="w-4 h-4 dark:text-white" aria-hidden="true" xmlns="http://www.w3.org/2000/svg"
                                fill="none" viewBox="0 0 16 20">
                                <path fill="currentColor"
                                    d="M11.045 7.514a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0Zm-4.572 3.072L3.857 15.92h7.949l-1.811-3.37-1.61 2.716-1.912-4.679Z" />
                                <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"
                                    stroke-width="2"
                                    d="M6 1v4a1 1 0 0 1-1 1H1m14 12a.97.97 0 0 1-.933 1H1.933A.97.97 0 0 1 1 18V5.828a2 2 0 0 1 .586-1.414l2.828-2.828A2 2 0 0 1 5.828 1h8.239A.97.97 0 0 1 15 2v16ZM11.045 7.514a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0ZM3.857 15.92l2.616-5.333 1.912 4.68 1.61-2.717 1.81 3.37H3.858Z" />
                            </svg>
                            <span id="text-label-input">{{(isset($info_bisnis[0]->logo)) ? 'Ubah' : 'Unggah'}}
                                logo</span>
                        </label>
                        <input id="logoImage" onchange="previewImageLogo()"
                            class="block hidden w-full text-sm text-gray-900 border border-gray-300 rounded-lg cursor-pointer bg-gray-50 dark:text-gray-400 focus:outline-none dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400"
                            type="file">

                        <button id="btnSubmitLogo"
                            class="primary-color hidden font-medium py-2 px-3 rounded-md text-sm text-white flex items-center gap-2">
                            <svg class="w-4 h-4 dark:text-white" aria-hidden="true" xmlns="http://www.w3.org/2000/svg"
                                fill="none" viewBox="0 0 16 20">
                                <path fill="currentColor"
                                    d="M11.045 7.514a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0Zm-4.572 3.072L3.857 15.92h7.949l-1.811-3.37-1.61 2.716-1.912-4.679Z" />
                                <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"
                                    stroke-width="2"
                                    d="M6 1v4a1 1 0 0 1-1 1H1m14 12a.97.97 0 0 1-.933 1H1.933A.97.97 0 0 1 1 18V5.828a2 2 0 0 1 .586-1.414l2.828-2.828A2 2 0 0 1 5.828 1h8.239A.97.97 0 0 1 15 2v16ZM11.045 7.514a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0ZM3.857 15.92l2.616-5.333 1.912 4.68 1.61-2.717 1.81 3.37H3.858Z" />
                            </svg>Simpan logo
                        </button>
                    </form>
                    <button data-modal-toggle="deleteLogoModal" id="btnRemoveLogo"
                        class="py-2 px-3 {{(isset($info_bisnis[0]->logo)) ? '' : 'hidden'}} font-medium text-sm text-gray-600 rounded-md border flex items-center gap-2">
                        <svg class="w-4 h-4 dark:text-white" aria-hidden="true" xmlns="http://www.w3.org/2000/svg"
                            fill="none" viewBox="0 0 18 20">
                            <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M1 5h16M7 8v8m4-8v8M7 1h4a1 1 0 0 1 1 1v3H6V2a1 1 0 0 1 1-1ZM3 5h12v13a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V5Z" />
                        </svg>
                        Hapus
                    </button>
                </div>
            </div>
        </div>


        <div class="bg-white rounded-md border shadow-sm border border-gray-200 p-5 my-5">
            <h3 class="text-xl font-semibold mb-3">Informasi bisnis</h3>

            <form action="/store_info_bisnis" method="POST">
                @csrf
                <div class="grid gap-6 mb-6 md:grid-cols-2">
                    <div>
                        <label for="nama_bisnis"
                            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Nama bisnis</label>
                        <input type="text" value="{{ isset($info_bisnis[0]) ? $info_bisnis[0]->nama_bisnis : '' }}"
                            id="nama_bisnis" name="nama_bisnis"
                            class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                            placeholder="Nama bisnis kamu" required>
                    </div>
                    <div>
                        <label for="alamat_bisnis"
                            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Alamat bisnis</label>
                        <input type="text" value="{{ isset($info_bisnis[0]) ? $info_bisnis[0]->alamat : '' }}"
                            id="alamat_bisnis" name="alamat"
                            class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                            placeholder="Alamat bisnis kamu" required>
                    </div>
                    <div>
                        <label for="jabatan"
                            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Jabatan</label>
                        <input type="text" id="jabatan"
                            value="{{ isset($info_bisnis[0]) ? $info_bisnis[0]->jabatan : '' }}" name="jabatan"
                            class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                            placeholder="Jabatan kamu di bisnis" required>
                    </div>
                    <div>
                        <label for="email_bisnis"
                            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Email</label>
                        <input value="{{ isset($info_bisnis[0]) ? $info_bisnis[0]->email : '' }}" type="email"
                            id="email_bisnis" name="email"
                            class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                            placeholder="Email bisnis kamu" required>
                    </div>
                    <div>
                        <label for="no_tax" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">No
                            TAX</label>
                        <input type="text" value="{{ isset($info_bisnis[0]) ? $info_bisnis[0]->no_tax : '' }}"
                            id="no_tax" name="no_tax"
                            class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                            placeholder="No TAX bisnis kamu">
                    </div>
                    <div>
                        <label for="website"
                            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Website</label>
                        <input type="text" id="website"
                            value="{{ isset($info_bisnis[0]) ? $info_bisnis[0]->website : '' }}" name="website"
                            class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                            placeholder="Website bisnis kamu" required>
                    </div>
                    <div class="md:col-span-2">
                        <label for="no_handphone_bisnis"
                            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Nomor handphone</label>
                        <input type="tel" value="{{ isset($info_bisnis[0]) ? $info_bisnis[0]->no_handphone : '' }}"
                            id="no_handphone_bisnis" name="no_handphone"
                            class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                            placeholder="No handphone kamu" required>
                    </div>
                </div>
                <button type="submit"
                    class="py-2 px-3 rounded-md text-white flex items-center gap-2 primary-color text-sm font-medium">
                    <svg class="w-4 h-4 dark:text-white" aria-hidden="true" xmlns="http://www.w3.org/2000/svg"
                        fill="none" viewBox="0 0 10 14">
                        <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                            d="M5 13V1m0 0L1 5m4-4 4 4" />
                    </svg>
                    Simpan
                </button>
            </form>

        </div>

    </div>


    </div>
    {{-- Informasi Bisnis End --}}

    {{-- fitur octans platform --}}
    <div class="grid grid-cols-1 xl:grid-cols-2 gap-4">
        <div
        class="col-span-1 bg-white shadow-sm border border-gray-200 p-4 dark:border-gray-700 sm:p-6 dark:bg-gray-800 rounded-md">
        <h3 class="text-xl font-semibold mb-2">Octans platform</h3>
        <p class="text-gray-600 mb-3">Octans platform memungkinkan para pelaku usaha mengelola sistem pembayaran mereka</p>

        @if (isset($octans_platform))
        <ol
            class="flex items-center w-full text-sm font-medium text-center text-gray-500 dark:text-gray-400 2xl:text-base">
            <li
                class="flex md:w-full items-center 2xl:after:content-[''] after:w-full after:h-1 after:border-b after:border-gray-200 after:border-1 after:hidden 2xl:after:inline-block after:mx-6 xl:after:mx-10 dark:after:border-gray-700">
                <span
                    class="flex items-center {{($octans_platform->status == 'invited' || $octans_platform->status == 'registered') ? 'text-[#7554ab]' : ''}} after:content-['/'] 2xl:after:hidden after:mx-2 after:text-gray-200 dark:after:text-gray-500">
                    <svg class="w-3.5 h-3.5 {{($octans_platform->status == 'invited' || $octans_platform->status == 'registered') ? '' : 'hidden'}}  2xl:w-4 2xl:h-4 me-2.5" aria-hidden="true" xmlns="http://www.w3.org/2000/svg"
                    fill="currentColor" viewBox="0 0 20 20">
                    <path
                        d="M10 .5a9.5 9.5 0 1 0 9.5 9.5A9.51 9.51 0 0 0 10 .5Zm3.707 8.207-4 4a1 1 0 0 1-1.414 0l-2-2a1 1 0 0 1 1.414-1.414L9 10.586l3.293-3.293a1 1 0 0 1 1.414 1.414Z" />
                    </svg>
                    <span class="me-2 {{($octans_platform->status == 'invited' || $octans_platform->status == 'registered') ? 'hidden' : ''}} ">1</span>
                    Aktifasi
                </span>
            </li>
            <li
                class="flex md:w-full items-center after:content-[''] after:w-full after:h-1 after:border-b after:border-gray-200 after:border-1 after:hidden 2xl:after:inline-block after:mx-6 xl:after:mx-10 dark:after:border-gray-700">
                <span
                    class="flex items-center {{($octans_platform->status == 'registered') ? 'text-[#7554ab]' : ''}} after:content-['/'] 2xl:after:hidden after:mx-2 after:text-gray-200 dark:after:text-gray-500">
                    <svg class="w-3.5 h-3.5 {{($octans_platform->status == 'registered') ? '' : 'hidden'}}  2xl:w-4 2xl:h-4 me-2.5" aria-hidden="true" xmlns="http://www.w3.org/2000/svg"
                        fill="currentColor" viewBox="0 0 20 20">
                        <path
                            d="M10 .5a9.5 9.5 0 1 0 9.5 9.5A9.51 9.51 0 0 0 10 .5Zm3.707 8.207-4 4a1 1 0 0 1-1.414 0l-2-2a1 1 0 0 1 1.414-1.414L9 10.586l3.293-3.293a1 1 0 0 1 1.414 1.414Z" />
                        </svg>
                    <span class="me-2  {{($octans_platform->status == 'registered') ? 'hidden' : ''}}">2</span>
                    Terdaftar
                </span>
            </li>

            <li
                class="flex md:w-full items-center after:content-[''] after:w-full after:h-1 after:border-b after:border-gray-200 after:border-1 after:hidden 2xl:after:inline-block after:mx-6 xl:after:mx-10 dark:after:border-gray-700">
                <span
                    class="flex items-center after:content-['/'] 2xl:after:hidden after:mx-2 after:text-gray-200 dark:after:text-gray-500">
                    <span class="me-2">3</span>
                    Verifikasi <span class="hidden 2xl:inline-flex 2xl:ms-2">dokumen</span>
                </span>
            </li>

            <li class="flex items-center">
                <span class="me-2">4</span>
                Aktif
            </li>
        </ol>
        @else
        <ol
            class="flex items-center w-full text-sm font-medium text-center text-gray-500 dark:text-gray-400 2xl:text-base">
            <li
                class="flex md:w-full items-center 2xl:after:content-[''] after:w-full after:h-1 after:border-b after:border-gray-200 after:border-1 after:hidden 2xl:after:inline-block after:mx-6 xl:after:mx-10 dark:after:border-gray-700">
                <span
                    class="flex items-center after:content-['/'] 2xl:after:hidden after:mx-2 after:text-gray-200 dark:after:text-gray-500">
                    {{-- <svg class="w-3.5 h-3.5 2xl:w-4 2xl:h-4 me-2.5" aria-hidden="true" xmlns="http://www.w3.org/2000/svg"
                        fill="currentColor" viewBox="0 0 20 20">
                        <path
                            d="M10 .5a9.5 9.5 0 1 0 9.5 9.5A9.51 9.51 0 0 0 10 .5Zm3.707 8.207-4 4a1 1 0 0 1-1.414 0l-2-2a1 1 0 0 1 1.414-1.414L9 10.586l3.293-3.293a1 1 0 0 1 1.414 1.414Z" />
                    </svg> --}}
                    <span class="me-2">1</span>
                    Aktifasi
                </span>
            </li>
            <li
                class="flex md:w-full items-center after:content-[''] after:w-full after:h-1 after:border-b after:border-gray-200 after:border-1 after:hidden 2xl:after:inline-block after:mx-6 xl:after:mx-10 dark:after:border-gray-700">
                <span
                    class="flex items-center after:content-['/'] 2xl:after:hidden after:mx-2 after:text-gray-200 dark:after:text-gray-500">
                    <span class="me-2">2</span>
                    Terdaftar
                </span>
            </li>

            <li
                class="flex md:w-full items-center after:content-[''] after:w-full after:h-1 after:border-b after:border-gray-200 after:border-1 after:hidden 2xl:after:inline-block after:mx-6 xl:after:mx-10 dark:after:border-gray-700">
                <span
                    class="flex items-center after:content-['/'] 2xl:after:hidden after:mx-2 after:text-gray-200 dark:after:text-gray-500">
                    <span class="me-2">3</span>
                    Verifikasi <span class="hidden 2xl:inline-flex sm:ms-2">dokumen</span>
                </span>
            </li>

            <li class="flex items-center">
                <span class="me-2">4</span>
                Aktif
            </li>
        </ol>
        @endif


        <button data-modal-target="{{(isset($octans_platform)) ? 'infoAktifasiModal' : 'confirmModal'}}" data-modal-toggle="{{(isset($octans_platform)) ? 'infoAktifasiModal' : 'confirmModal'}}"
            class="py-2 px-3 rounded-md primary-color text-white mt-4 cursor-pointer">
            Aktifkan
        </button>
        </div>
        <div class="bg-white rounded-md border shadow-sm border border-gray-200 p-5 col-span-1">
            <h3 class="text-xl font-semibold mb-3">Metode pembayaran</h3>

            <div class="border-y py-3 flex items-center gap-2">
               <div class="flex items-center gap-2">
                <p class="text-lg">QRIS</p>
                <p class="text-sm text-gray-500 italic">(Tidak aktif)</p>
               </div>
               <button class="text-white primary-color hover:bg-gray-200 rounded-lg text-sm py-2 px-3 ml-auto inline-flex items-center dark:hover:bg-gray-600 dark:hover:text-white">
                Aktfikan
               </button>
            </div>
        </div>
    </div>
    {{-- fitur octans platform end --}}



    {{-- <div class="grid grid-cols-1 xl:grid-cols-2 xl:gap-4">
        <div
            class="p-4 mb-3 bg-white  rounded-lg shadow-sm border border-gray-200 dark:border-gray-700 sm:p-6 dark:bg-gray-800 xl:mb-0">
            <div class="flow-root">
                <h3 class="text-xl font-semibold dark:text-white">Octans platform</h3>
                <p class="text-sm text-gray-500 dark:text-gray-400">Anda dapat mengaktifkan fitur Pembayaran untuk
                    bisnis anda</p>
                <div class="divide-y divide-gray-200 dark:divide-gray-700">
                    <!-- Item 1 -->
                    <div class="flex items-center justify-between py-4">
                        <div class="flex flex-col flex-grow">
                            <div class="text-lg font-semibold text-gray-900 dark:text-white">Qris</div>
                            <div class="text-sm text-gray-500 dark:text-gray-400">Aktifkan Qris untuk kemudahan
                                transaksi anda</div>
                        </div>
                        <label class="relative flex items-center cursor-pointer">
                            <input type="checkbox" value="" class="sr-only peer">
                            <div
                                class="w-11 h-6 bg-gray-200 peer-focus:outline-none dark:peer-focus:ring-[#5d369c] rounded-full peer dark:bg-gray-700 peer-checked:after:translate-x-full rtl:peer-checked:after:-translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-[2px] after:start-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:w-5 after:h-5 after:transition-all dark:border-gray-600 peer-checked:bg-[#5d369c]">
                            </div>
                        </label>
                    </div>
                    <!-- Item 2 -->
                    <div class="flex items-center justify-between py-4">
                        <div class="flex flex-col flex-grow">
                            <div class="text-lg font-semibold text-gray-900 dark:text-white">Account Activity</div>
                            <div class="text-sm text-gray-500 dark:text-gray-400">Get important notifications about you
                                or activity you've missed</div>
                        </div>
                        <label class="relative flex items-center cursor-pointer">
                            <input type="checkbox" value="" class="sr-only peer">
                            <div
                                class="w-11 h-6 bg-gray-200 peer-focus:outline-none dark:peer-focus:ring-[#5d369c] rounded-full peer dark:bg-gray-700 peer-checked:after:translate-x-full rtl:peer-checked:after:-translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-[2px] after:start-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:w-5 after:h-5 after:transition-all dark:border-gray-600 peer-checked:bg-[#5d369c]">
                            </div>
                        </label>
                    </div>
                    <!-- Item 3 -->
                    <div class="flex items-center justify-between py-4">
                        <div class="flex flex-col flex-grow">
                            <div class="text-lg font-semibold text-gray-900 dark:text-white">Meetups Near You</div>
                            <div class="text-sm text-gray-500 dark:text-gray-400">Get an email when a Dribbble Meetup is
                                posted close to my location</div>
                        </div>
                        <label class="relative flex items-center cursor-pointer">
                            <input type="checkbox" value="" class="sr-only peer">
                            <div
                                class="w-11 h-6 bg-gray-200 peer-focus:outline-none dark:peer-focus:ring-[#5d369c] rounded-full peer dark:bg-gray-700 peer-checked:after:translate-x-full rtl:peer-checked:after:-translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-[2px] after:start-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:w-5 after:h-5 after:transition-all dark:border-gray-600 peer-checked:bg-[#5d369c]">
                            </div>
                        </label>
                    </div>
                    <!-- Item 4 -->
                    <div class="flex items-center justify-between pt-4">
                        <div class="flex flex-col flex-grow">
                            <div class="text-lg font-semibold text-gray-900 dark:text-white">New Messages</div>
                            <div class="text-sm text-gray-500 dark:text-gray-400">Get Themsberg news, announcements, and
                                product updates</div>
                        </div>
                        <label class="relative flex items-center cursor-pointer">
                            <input type="checkbox" value="" class="sr-only peer">
                            <div
                                class="w-11 h-6 bg-gray-200 peer-focus:outline-none dark:peer-focus:ring-[#5d369c] rounded-full peer dark:bg-gray-700 peer-checked:after:translate-x-full rtl:peer-checked:after:-translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-[2px] after:start-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:w-5 after:h-5 after:transition-all dark:border-gray-600 peer-checked:bg-[#5d369c]">
                            </div>
                        </label>
                    </div>
                </div>
                <div class="mt-6">
                    <button
                        class="py-2 px-3 rounded-md text-white flex items-center gap-2 primary-color text-sm font-medium">
                        <svg class="w-4 h-4 dark:text-white" aria-hidden="true" xmlns="http://www.w3.org/2000/svg"
                            fill="none" viewBox="0 0 10 14">
                            <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M5 13V1m0 0L1 5m4-4 4 4" />
                        </svg>
                        Simpan
                    </button>
                </div>
            </div>
        </div>
        <div
            class="p-4 mb-4 bg-white  rounded-lg shadow-sm border border-gray-200 dark:border-gray-700 sm:p-6 dark:bg-gray-800 xl:mb-0">
            <div class="flow-root">
                <h3 class="text-xl font-semibold dark:text-white">Email Notifications</h3>
                <p class="text-sm text-gray-500 dark:text-gray-400">You can set up Themesberg to get email notifications
                </p>
                <div class="divide-y divide-gray-200 dark:divide-gray-700">
                    <!-- Item 1 -->
                    <div class="flex items-center justify-between py-4">
                        <div class="flex flex-col flex-grow">
                            <div class="text-lg font-semibold text-gray-900 dark:text-white">Rating reminders</div>
                            <div class="text-sm text-gray-500 dark:text-gray-400">Send an email reminding me to rate an
                                item a week after purchase</div>
                        </div>
                        <label class="relative flex items-center cursor-pointer">
                            <input type="checkbox" value="" class="sr-only peer">
                            <div
                                class="w-11 h-6 bg-gray-200 peer-focus:outline-none dark:peer-focus:ring-[#5d369c] rounded-full peer dark:bg-gray-700 peer-checked:after:translate-x-full rtl:peer-checked:after:-translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-[2px] after:start-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:w-5 after:h-5 after:transition-all dark:border-gray-600 peer-checked:bg-[#5d369c]">
                            </div>
                        </label>
                    </div>
                    <!-- Item 2 -->
                    <div class="flex items-center justify-between py-4">
                        <div class="flex flex-col flex-grow">
                            <div class="text-lg font-semibold text-gray-900 dark:text-white">Item update notifications
                            </div>
                            <div class="text-sm text-gray-500 dark:text-gray-400">Send user and product notifications
                                for you</div>
                        </div>
                        <label class="relative flex items-center cursor-pointer">
                            <input type="checkbox" value="" class="sr-only peer">
                            <div
                                class="w-11 h-6 bg-gray-200 peer-focus:outline-none dark:peer-focus:ring-[#5d369c] rounded-full peer dark:bg-gray-700 peer-checked:after:translate-x-full rtl:peer-checked:after:-translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-[2px] after:start-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:w-5 after:h-5 after:transition-all dark:border-gray-600 peer-checked:bg-[#5d369c]">
                            </div>
                        </label>
                    </div>
                    <!-- Item 3 -->
                    <div class="flex items-center justify-between py-4">
                        <div class="flex flex-col flex-grow">
                            <div class="text-lg font-semibold text-gray-900 dark:text-white">Item comment notifications
                            </div>
                            <div class="text-sm text-gray-500 dark:text-gray-400">Send me an email when someone comments
                                on one of my items</div>
                        </div>
                        <label class="relative flex items-center cursor-pointer">
                            <input type="checkbox" value="" class="sr-only peer">
                            <div
                                class="w-11 h-6 bg-gray-200 peer-focus:outline-none dark:peer-focus:ring-[#5d369c] rounded-full peer dark:bg-gray-700 peer-checked:after:translate-x-full rtl:peer-checked:after:-translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-[2px] after:start-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:w-5 after:h-5 after:transition-all dark:border-gray-600 peer-checked:bg-[#5d369c]">
                            </div>
                        </label>
                    </div>
                    <!-- Item 4 -->
                    <div class="flex items-center justify-between pt-4">
                        <div class="flex flex-col flex-grow">
                            <div class="text-lg font-semibold text-gray-900 dark:text-white">Buyer review notifications
                            </div>
                            <div class="text-sm text-gray-500 dark:text-gray-400">Send me an email when someone leaves a
                                review with their rating</div>
                        </div>
                        <label class="relative flex items-center cursor-pointer">
                            <input type="checkbox" value="" class="sr-only peer">
                            <div
                                class="w-11 h-6 bg-gray-200 peer-focus:outline-none dark:peer-focus:ring-[#5d369c] rounded-full peer dark:bg-gray-700 peer-checked:after:translate-x-full rtl:peer-checked:after:-translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-[2px] after:start-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:w-5 after:h-5 after:transition-all dark:border-gray-600 peer-checked:bg-[#5d369c]">
                            </div>
                        </label>
                    </div>
                </div>
                <div class="mt-6">
                    <button
                        class="py-2 px-3 rounded-md text-white flex items-center gap-2 primary-color text-sm font-medium">
                        <svg class="w-4 h-4 dark:text-white" aria-hidden="true" xmlns="http://www.w3.org/2000/svg"
                            fill="none" viewBox="0 0 10 14">
                            <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M5 13V1m0 0L1 5m4-4 4 4" />
                        </svg>
                        Simpan
                    </button>
                </div>
            </div>
        </div>
    </div> --}}



    {{-- informasi password --}}

    <div id="change-password"
        class="p-4 lg:my-5 bg-white  rounded-lg shadow-sm border border-gray-200 2xl:col-span-2 dark:border-gray-700 sm:p-6 dark:bg-gray-800">
        <h3 class="mb-4 text-xl font-semibold dark:text-white">Ubah password</h3>
        <form action="/ubah-password" method="post">
            @csrf
            <div class="grid grid-cols-1 lg:grid-cols-3 gap-5">
                <div class="">
                    <label for="current-password"
                        class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Password saat ini</label>
                    <input type="password" value="" name="password_sekarang" id="current-password"
                        class="shadow-sm border border-gray-200 bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-500 focus:border-primary-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500"
                        placeholder="••••••••" required>
                    @if (session()->has('error'))
                    <p class="text-red-700 text-sm mt-1">Password yang anda masukkan salah</p>
                    @endif
                </div>
                <div class="">
                    <label for="password" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Buat
                        password baru</label>
                    <input data-popover-target="popover-password" data-popover-placement="bottom" type="password"
                        id="password" name="password"
                        class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                        placeholder="••••••••" required>
                    @if (session()->has('confirm-password'))
                    <p class="text-red-700 text-sm mt-1">Password yang anda masukkan tidak sama</p>
                    @endif
                </div>
                <div class="">
                    <label for="confirm-password"
                        class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Konfirmasi password</label>
                    <input type="password" name="password_confirmation" id="confirm-password"
                        class="shadow-sm border border-gray-200 bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-500 focus:border-primary-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500"
                        placeholder="••••••••" required>
                </div>
                <div>
                    <button type="submit"
                        class="py-2 px-3 rounded-md text-white flex items-center gap-2 primary-color text-sm font-medium">
                        <svg class="w-4 h-4 dark:text-white" aria-hidden="true" xmlns="http://www.w3.org/2000/svg"
                            fill="none" viewBox="0 0 10 14">
                            <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M5 13V1m0 0L1 5m4-4 4 4" />
                        </svg>
                        Simpan
                    </button>
                </div>
            </div>
        </form>
    </div>

    {{-- informasi password End --}}

    </div>
</section>

    <script>
        const logoImage = document.getElementById('logoImage')
        const avatar = document.getElementById('avatar')
        const inputLogo = document.getElementById('inputLogo')
        const labelInput = document.getElementById('labelInput')
        const btnSubmitLogo = document.getElementById("btnSubmitLogo")
        const textLabelInput = document.getElementById('text-label-input')
        const btnRemoveLogo = document.getElementById('btnRemoveLogo')
        const previewImageLogo = () => {
            inputLogo.files = logoImage.files
            const file = inputLogo.files[0]; // Mengambil file dari input
            textLabelInput.textContent = 'Ubah logo'
            btnSubmitLogo.classList.remove('hidden')
            btnRemoveLogo.classList.add('hidden')
            labelInput.classList.remove('primary-color')
            labelInput.classList.remove('text-white')
            labelInput.classList.add('text-gray-700')
            labelInput.classList.add('border')
            labelInput.classList.add('border-gray-600')
            if (file) {
                const imageUrl = URL.createObjectURL(file); // Membuat URL objek dari file
                avatar.src = imageUrl; // Menampilkan gambar dalam elemen img
            }
        }

        const modalBackdrop = document.getElementById('modal-backdrop')
        const suksesModal = document.getElementById('successModal')
        const modalHidden = () => {
            modalBackdrop.classList.toggle('hidden')
            suksesModal.classList.toggle('hidden')
        }

        const modalBackdropError = document.getElementById('modal-backdrop-error')
        const modalError = document.getElementById('modalError')
        const modalErrorHidden = () => {
            modalBackdropError.classList.toggle('hidden');
            modalError.classList.toggle('hidden')
        }
    </script>
@endsection
