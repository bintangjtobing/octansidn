<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>QRIS PT Boxity Central Indonesia</title>
    <script src="https://cdn.tailwindcss.com"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/flowbite/2.0.0/flowbite.min.css" rel="stylesheet" />
</head>
<body class="flex items-center justify-center " style="background-image: url(image/background/bacground-qris.png); background-size: cover">

    <section class="" style="margin-top: 12rem">
        <div class="text-center" style="margin-bottom: 3rem">
            <h1 class="text-4xl font-bold">PT Boxity Central Indonesia</h1>
            <p class="font-light text-lg" style="color: #4B5563;">XOID : 45KDJHVDJ4HGNVJNN4KKXJCJKDK</p>
        </div>

        <div class="visible-print text-center">
            <img src="{{ $qrCodePath }}" alt="QrCode Image">
            {{-- <p>Scan me to return to the original page.</p> --}}
        </div>
    </section>


<script src="https://cdnjs.cloudflare.com/ajax/libs/flowbite/2.0.0/flowbite.min.js"></script>
</body>
</html>
