<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="icon" href="./image/logo/octansidnByBoxity.png">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;200;300;400;500;600;700;800;900&display=swap">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <title style="text-transform: capitalize;">{{ Route::currentRouteName() }} - Octans by Boxity</title>
    <link rel="shortcut icon" href="https://www.octansidn.com/webpage/demos/business/images/octans/icon_2.ico"
        type="image/x-icon">
    <style>
        .grecaptcha-badge {
            visibility: hidden !important
        }

        .primary-color {
            background: hsla(263, 49%, 41%, 1);
            background: linear-gradient(45deg, hsla(263, 49%, 41%, 1)0%, hsla(290, 41%, 45%, 1)52%, hsla(327, 59%, 57%, 1)100%);
            background: -webkit-linear-gradient(45deg, hsla(263, 49%, 41%, 1)0%, hsla(290, 41%, 45%, 1)52%, hsla(327, 59%, 57%, 1)100%);
        }

        .primary-color:hover {
            background: linear-gradient(45deg, hsla(263, 49%, 31%, 1)0%, hsla(290, 41%, 35%, 1)52%, hsla(327, 59%, 47%, 1)100%)
        }

        .text-primary-color {
            color: #d24f98
        }

        * {
            font-family: 'Roboto', sans-serif
        }
    </style>
    {!! RecaptchaV3::initJs() !!}
</head>

<body class="font-body">
    <div style="min-height: 100vh" class="antialiased bg-gray-50 dark:bg-gray-900">
        <div>
            @yield('container')
        </div>
    </div>
</body>

</html>
