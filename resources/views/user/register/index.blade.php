@extends('user.layouts.main_layouts')

@section('container')
    <style>
        .background {
            background-image: url('https://res.cloudinary.com/du0tz73ma/image/upload/w_1000/q_auto:best/f_auto/v1703068124/octansidn-login_cghluk.svg');
            background-size: cover;
            background-repeat: no-repeat
        }
    </style>
    <section class="dark:bg-gray-900 background" style="background-color:#F6F2F4;">

        <div class="flex flex-col items-center justify-center px-6 py-8 mx-auto h-screen md:h-screen lg:py-0">
            <a href="#" class="flex items-center mb-6 text-2xl font-semibold text-gray-900 dark:text-white">
                <img class="h-16 mr-2"
                    src="https://res.cloudinary.com/du0tz73ma/image/upload/w_1000/q_auto:best/f_auto/v1702445620/octansidnByBoxity_vwv8wi.png"
                    alt="OctansIDN Logo Official">
            </a>
            <div
                class="w-full bg-white rounded-lg shadow dark:border md:mt-0 sm:max-w-md xl:p-0 dark:bg-gray-800 dark:border-gray-700">
                <div class="p-6 space-y-4 md:space-y-6 sm:p-8">
                    <h1 class="text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-2xl dark:text-white">
                        Masuk dan daftar
                    </h1>
                    <form class="space-y-4 md:space-y-6" method="POST" action="/register">
                        @csrf
                        <div>
                            <label for="username"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Username</label>
                            <input type="text" name="username" id="username"
                                class="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-[#9345a3] focus:border-[#9345a3] block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-[#9345a3] dark:focus:border-[#9345a3]"
                                placeholder="username" required="">
                            @if ($errors->has('username'))
                                <p class="mt-2 text-sm text-red-600 dark:text-red-500"><span
                                        class="font-medium">Sorry,</span> {{ $errors->first('username') }}</p>
                            @endif

                        </div>
                        <div>
                            <label for="email"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Email</label>
                            <input type="email" name="email" id="email"
                                class="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-[#9345a3] focus:border-[#9345a3] block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-[#9345a3] dark:focus:border-[#9345a3]"
                                placeholder="name@company.com" required="">
                            @if ($errors->has('email'))
                                <p class="mt-2 text-sm text-red-600 dark:text-red-500"><span
                                        class="font-medium">Sorry,</span> {{ $errors->first('email') }}</p>
                            @endif
                        </div>
                        <div>
                            <label for="password" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Kata
                                Sandi</label>
                            <input type="password" name="password" id="password" placeholder="••••••••"
                                class="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-[#9345a3] focus:border-[#9345a3] block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-[#9345a3] dark:focus:border-[#9345a3]"
                                required="">
                        </div>
                        <div>
                            <label for="confirm-password"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Konfirmasi Kata
                                Sandi</label>
                            <input type="password" name="confirm-password" id="confirm-password" placeholder="••••••••"
                                class="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-[#9345a3] focus:border-[#9345a3] block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-[#9345a3] dark:focus:border-[#9345a3]"
                                required="">
                        </div>
                        <button type="submit"
                            class="primary-color w-full text-white focus:ring-4 focus:outline-none focus:ring-[#d24f98] font-medium rounded-lg text-sm px-5 py-2.5 text-center">Masuk
                            dan daftar</button>
                        <p class="text-sm font-light text-gray-500 dark:text-gray-400">
                            Sudah Punya Akun? <a href="/login"
                                class="text-primary-color font-medium text-primary-600 hover:underline dark:text-primary-500">Masuk
                                Disini</a>
                        </p>
                    </form>
                </div>
            </div>
            <p class="text-sm text-gray-700 my-5">© Copyright <?php $Y = date('Y');
            echo $Y; ?> - OctansIDN | Reserved By <abbr
                    title="PT Boxity Central Indonesia">BoxityID</abbr></p>
        </div>
    </section>
@endsection
