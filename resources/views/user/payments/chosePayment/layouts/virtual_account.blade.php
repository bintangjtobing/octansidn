@extends('user.layouts.main_layouts')

@section('container')

    {{-- modal sukses--}}
    <div id="successModal" tabindex="-1" aria-hidden="true" class="hidden overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0 z-50 justify-center items-center w-full md:inset-0 h-modal md:h-full">
        <div class="relative p-4 w-full max-w-md h-full md:h-auto">
            <!-- Modal content -->
            <div class="relative p-4 text-center bg-white rounded-lg shadow dark:bg-gray-800 sm:p-5">
                <button type="button" class="text-gray-400 absolute top-2.5 right-2.5 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-600 dark:hover:text-white" data-modal-toggle="successModal">
                    <svg aria-hidden="true" class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
                    <span class="sr-only">Close modal</span>
                </button>
                <div class="w-12 h-12 rounded-full bg-green-100 dark:bg-green-900 p-2 flex items-center justify-center mx-auto mb-3.5">
                    <svg aria-hidden="true" class="w-8 h-8 text-green-500 dark:text-green-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd"></path></svg>
                    <span class="sr-only">Sukses</span>
                </div>
                <p class="mb-4 text-lg font-semibold text-gray-900 dark:text-white">Pembayaaran octans  finance anda berhasil</p>
                <a href="/" class="py-2 px-3 text-sm font-medium text-center text-white rounded-lg bg-primary-600 hover:bg-primary-700 focus:ring-4 focus:outline-none focus:ring-primary-300 dark:focus:ring-primary-900">
                    Lanjut ke dashboard
                </a>
            </div>
        </div>
    </div>
    {{-- modal sukses end --}}

    <section class="bg-50 h-screen w-screen flex justify-center items-center">
        <div class="px-7 py-7 rounded-md shadow-sm border border-gray-200 flex flex-col items-center justify-center">
            <a href="#" class="flex items-center mb-2 text-2xl font-semibold text-gray-900 dark:text-white">
                <img class="h-20 mr-2"
                    src="https://res.cloudinary.com/du0tz73ma/image/upload/w_1000/q_auto:best/f_auto/v1702445620/octansidnByBoxity_vwv8wi.png"
                    alt="OctansIDN Logo Official">
            </a>
            <div class="w-80">
                <div class="text-center text-red-600 font-semibold text-xl mb-3" id="countdown"></div>
                <p class="mb-3 class text-gray-700">Transfer ke virtual account</p>
                <p class="mb-2 uppercase">{{ $sender_bank }}</p>
                <div class="bg-gray-200 px-5 py-3 rounded-md">
                    <p class="text-gray-600 font-semibold">{{ $noVa }}</p>
                </div>
                <p class="my-3">Nominal Transafer</p>
                <div class="bg-gray-200 px-5 py-3 rounded-md">
                    <p class="text-gray-600 font-semibold">Rp {{ number_format($amount, 0, ',', '.') }}</p>
                </div>
            </div>
        </div>
        <div id="backdrop" modal-backdrop="" class="bg-gray-900 hidden bg-opacity-50 dark:bg-opacity-80 fixed inset-0 z-40"></div>
    </section>

    <script>

        const modalSukses = document.getElementById('successModal')
        const backdrop = document.getElementById('backdrop')

        function checkPaymentStatus() {
            fetch('/check-payment-status') // Endpoint untuk memeriksa status pembayaran
                .then(response => response.text())
                .then(data => {
                    if (data == 'successful') {
                        modalSukses.classList.replace('hidden', 'flex')
                        backdrop.classList.toggle('hidden')
                    } else {
                        setTimeout(checkPaymentStatus, 5000); // Cek lagi setelah 5 detik
                    }
                })
                .catch(error => {
                    console.error('Error checking payment status:', error);
                    setTimeout(checkPaymentStatus, 5000); // Cek lagi setelah 5 detik setelah kesalahan
                });
        }

        // Panggil fungsi pertama kali
        checkPaymentStatus();

        document.addEventListener('DOMContentLoaded', function() {
            const countdownElement = document.getElementById('countdown');
            let countdownDuration = 600; // Durasi countdown dalam detik (10 menit)

            function updateCountdown() {
                const minutes = Math.floor(countdownDuration / 60);
                const seconds = countdownDuration % 60;

                countdownElement.innerText = `${minutes}:${seconds < 10 ? '0' : ''}${seconds}`;

                if (countdownDuration > 0) {
                    countdownDuration--;
                } else {
                    clearInterval(countdownInterval);
                    countdownElement.innerText = 'Waktu Habis!';
                    window.location.href = '/chose-payment-methode';
                }
            }

            // Pertama kali panggil fungsi updateCountdown
            updateCountdown();

            // Selanjutnya, update countdown setiap detik
            const countdownInterval = setInterval(updateCountdown, 1000);
        });
    </script>
@endsection
