@extends('dashboard.layouts.main')

@section('container')
    <div class="bg-white overflow-x-auto shadow-sm border border-gray-200 rounded-lg border-gray-300 dark:border-gray-600 h-fit">
        @include('dashboard.panduan.content')
    </div>
@endsection
