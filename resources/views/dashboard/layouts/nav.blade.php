<nav
    class="bg-white border-b border-gray-200 px-4 py-2.5 dark:bg-gray-800 dark:border-gray-700 fixed left-0 right-0 top-0 z-50">
    <div class="flex flex-wrap justify-between items-center">
        <div class="flex justify-start items-center">

            <button id="btn-toggle-sidebar" class="p-2 rounded-lg hover:border hidden md:block">
                <svg class="w-5 h-5 text-gray-500 dark:text-white" aria-hidden="true" xmlns="http://www.w3.org/2000/svg"
                    width="16" height="12" fill="none" viewBox="0 0 16 12">
                    <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                        d="M1 1h14M1 6h14M1 11h7" />
                </svg>
            </button>

            <button data-drawer-target="default-sidebar" data-drawer-toggle="default-sidebar" aria-expanded="true"
                aria-controls="sidebar"
                class="p-2 mr-2 text-gray-600 rounded-lg cursor-pointer md:hidden hover:text-gray-900 hover:bg-gray-100 focus:bg-gray-100 dark:focus:bg-gray-700 focus:ring-2 focus:ring-gray-100 dark:focus:ring-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white">
                <svg class="w-[18px] h-[18px]" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none"
                    viewBox="0 0 17 14">
                    <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                        d="M1 1h15M1 7h15M1 13h15" />
                </svg>
                <span class="sr-only">Toggle sidebar</span>
            </button>

            <div class="flex ms-1 lg:ms-8">
                <figure class="flex items-end gap-2">
                    @if (file_exists(public_path('./image/logo/octans_logo.png')))
                        <a href="/"><img class="h-10 md:h-12"
                                src="https://res.cloudinary.com/du0tz73ma/image/upload/w_1000/q_auto:best/f_auto/v1702445620/octansidnByBoxity_vwv8wi.png"
                                alt="Octans IDN Logo"></a>
                    @else
                        <a href="/"><img class="h-10 md:h-12"
                                src="https://res.cloudinary.com/du0tz73ma/image/upload/w_1000/q_auto:best/f_auto/v1702445620/octansidnByBoxity_vwv8wi.png"
                                alt="Octans IDN Logo"></a>
                    @endif
                </figure>
            </div>

        </div>
        <div class="flex items-center lg:order-2">
            <div class="flex items-center gap-2">
                <div class="text-sm hidden lg:flex flex-col text-right  ">
                    <p class="font-semibold">
                        {{ isset(auth()->user()->nama) ? auth()->user()->nama : auth()->user()->username  ?? '--'}}</p>
                    <p class="text-gray-600">{{ auth()->user()->email ?? '--'}}</p>
                </div>
                <button type="button"
                    class="flex mx-3 text-sm rounded-full md:mr-0 focus:ring-4 focus:ring-gray-300 dark:focus:ring-gray-600"
                    id="user-menu-button" aria-expanded="false" data-dropdown-toggle="dropdown">
                    <span class="sr-only">Open user menu</span>
                    <img id="avatarButton" type="button" style="object-fit: cover;" data-dropdown-toggle="userDropdown"
                        data-dropdown-placement="bottom-start" class="w-10 h-10 rounded-full cursor-pointer"
                        src="{{ $user->foto ?? 'https://res.cloudinary.com/du0tz73ma/image/upload/w_1000/q_auto:best/f_auto/v1700278579/default-profile_y2huqf.jpg' }}"
                        alt="User dropdown">

                </button>

            </div>
            <!-- Dropdown menu -->
            <div class="hidden z-50 my-4 w-56 text-base list-none bg-white rounded divide-y divide-gray-100 shadow dark:bg-gray-700 dark:divide-gray-600 rounded-xl"
                id="dropdown">
                <div class="py-3 px-4">
                    <span
                        class="block text-sm font-semibold text-gray-900 dark:text-white">{{ auth()->user()->nama ?? '--' }}</span>
                    <span
                        class="block text-sm text-gray-900 truncate dark:text-white">{{ auth()->user()->email ?? '--'}}</span>
                </div>

                <ul class="py-1 text-gray-700 dark:text-gray-300" aria-labelledby="dropdown">
                    <li class="w-full px-4 flex gap-2 items-center hover:bg-gray-200">
                        <svg class="w-3 h-3 text-gray-800 dark:text-white" aria-hidden="true"
                            xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 18">
                            <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M4.109 17H1v-2a4 4 0 0 1 4-4h.87M10 4.5a3.5 3.5 0 1 1-7 0 3.5 3.5 0 0 1 7 0Zm7.95 2.55a2 2 0 0 1 0 2.829l-6.364 6.364-3.536.707.707-3.536 6.364-6.364a2 2 0 0 1 2.829 0Z" />
                        </svg>
                        <a class="py-2 text-base w-full" href="/profile">Profile</a>
                    </li>
                    <li class="w-full px-4 flex gap-2 items-center hover:bg-gray-200">
                        <svg class="w-3 h-3 text-gray-800 dark:text-white" aria-hidden="true"
                            xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 18">
                            <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M5 5h9M5 9h5m8-8H2a1 1 0 0 0-1 1v10a1 1 0 0 0 1 1h4l3.5 4 3.5-4h5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1Z" />
                        </svg>
                        <a class="py-2 text-base w-full" href="/feedback">Bantuan</a>
                    </li>
                    <li class="w-full px-4 flex gap-2 items-center hover:bg-gray-200">
                        <svg class="w-3 h-3 text-gray-800 dark:text-white" aria-hidden="true"
                            xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 11 20">
                            <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M1.75 15.363a4.954 4.954 0 0 0 2.638 1.574c2.345.572 4.653-.434 5.155-2.247.502-1.813-1.313-3.79-3.657-4.364-2.344-.574-4.16-2.551-3.658-4.364.502-1.813 2.81-2.818 5.155-2.246A4.97 4.97 0 0 1 10 5.264M6 17.097v1.82m0-17.5v2.138" />
                        </svg>
                        <a class="py-2 text-base w-full" href="/billings">Billing</a>
                    </li>
                    <li>
                        <form class="w-full hover:bg-gray-200 py-2" method="POST" action="/logout">
                            @csrf
                            <button class="flex items-center py- gap-2 ml-4 w-full" type="submit"
                                class="block py-2 px-4 w-full text-start text-sm hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white">
                                <svg class="w-3 h-3 text-gray-800 dark:text-white" aria-hidden="true"
                                    xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 18 15">
                                    <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"
                                        stroke-width="2"
                                        d="M1 7.5h11m0 0L8 3.786M12 7.5l-4 3.714M12 1h3c.53 0 1.04.196 1.414.544.375.348.586.82.586 1.313v9.286c0 .492-.21.965-.586 1.313A2.081 2.081 0 0 1 15 14h-3" />
                                </svg> Log Out</button>
                        </form>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
