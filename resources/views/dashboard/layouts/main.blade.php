<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link rel="icon" href="./image/logo/octans_logo.png" type="image/png">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;200;300;400;500;600;700;800;900&display=swap"
        rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="https://www.octansidn.com/webpage/demos/business/images/octans/icon_2.ico"
        type="image/x-icon">
    <title>
        {{ Route::currentRouteName() == '/' ? 'Dashboard - Octans by Boxity' : Route::currentRouteName() . ' - Octans by Boxity' }}
    </title>
    <script src="https://cdn.jsdelivr.net/npm/feather-icons/dist/feather.min.js"></script>
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.8.0/datepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://unpkg.com/trix@2.0.0/dist/trix.css">
    <script type="text/javascript" src="https://unpkg.com/trix@2.0.0/dist/trix.umd.min.js"></script>
    <style>
        * {
            font-family: 'Roboto', sans-serif;
        }

        .primary-color {
            background: hsla(263, 49%, 41%, 1);

            background: linear-gradient(45deg, hsla(263, 49%, 41%, 1) 0%, hsla(290, 41%, 45%, 1) 52%, hsla(327, 59%, 57%, 1) 100%);

            background: -moz-linear-gradient(45deg, hsla(263, 49%, 41%, 1) 0%, hsla(290, 41%, 45%, 1) 52%, hsla(327, 59%, 57%, 1) 100%);

            background: -webkit-linear-gradient(45deg, hsla(263, 49%, 41%, 1) 0%, hsla(290, 41%, 45%, 1) 52%, hsla(327, 59%, 57%, 1) 100%);

            filter: progid: DXImageTransform.Microsoft.gradient(startColorstr="#5D369C", endColorstr="#9345A3", GradientType=1);
        }

        .primary-color-sidebar-active {
            background: hsla(263, 49%, 41%, 1);

            background: linear-gradient(45deg, hsla(263, 49%, 41%, 1) 0%, hsla(290, 41%, 45%, 1) 52%, hsla(327, 59%, 57%, 1) 100%);

            background: -moz-linear-gradient(45deg, hsla(263, 49%, 41%, 1) 0%, hsla(290, 41%, 45%, 1) 52%, hsla(327, 59%, 57%, 1) 100%);

            background: -webkit-linear-gradient(45deg, hsla(263, 49%, 41%, 1) 0%, hsla(290, 41%, 45%, 1) 52%, hsla(327, 59%, 57%, 1) 100%);

            filter: progid: DXImageTransform.Microsoft.gradient(startColorstr="#5D369C", endColorstr="#9345A3", GradientType=1);

            color: white
        }


        .primary-color-sidebar-hover{
            background-color: white;
            /* color: #374151 */
        }

        .primary-color-sidebar-hover:hover{
            background: hsla(263, 49%, 41%, 1);

            background: linear-gradient(45deg, hsla(263, 49%, 41%, 1) 0%, hsla(290, 41%, 45%, 1) 52%, hsla(327, 59%, 57%, 1) 100%);

            background: -moz-linear-gradient(45deg, hsla(263, 49%, 41%, 1) 0%, hsla(290, 41%, 45%, 1) 52%, hsla(327, 59%, 57%, 1) 100%);

            background: -webkit-linear-gradient(45deg, hsla(263, 49%, 41%, 1) 0%, hsla(290, 41%, 45%, 1) 52%, hsla(327, 59%, 57%, 1) 100%);

            filter: progid: DXImageTransform.Microsoft.gradient(startColorstr="#5D369C", endColorstr="#9345A3", GradientType=1);

            color: white
        }

        .primary-color:hover {
            background: linear-gradient(45deg, hsla(263, 49%, 31%, 1) 0%, hsla(290, 41%, 35%, 1) 52%, hsla(327, 59%, 47%, 1) 100%);
        }


        .primary-color-text {
    /* Fallback color */
            color: hsla(263, 49%, 41%, 1);

            /* Standard syntax */
            background: linear-gradient(45deg, hsla(263, 49%, 41%, 1) 0%, hsla(290, 41%, 45%, 1) 52%, hsla(327, 59%, 57%, 1) 100%);

            /* Mozilla Firefox */
            background: -moz-linear-gradient(45deg, hsla(263, 49%, 41%, 1) 0%, hsla(290, 41%, 45%, 1) 52%, hsla(327, 59%, 57%, 1) 100%);

            /* Webkit (Safari/Chrome) */
            background: -webkit-linear-gradient(45deg, hsla(263, 49%, 41%, 1) 0%, hsla(290, 41%, 45%, 1) 52%, hsla(327, 59%, 57%, 1) 100%);

            /* Internet Explorer */
            filter: progid: DXImageTransform.Microsoft.gradient(startColorstr="#5D369C", endColorstr="#9345A3", GradientType=1);
        }


        .text-primary-color {
            color: #d24f98;
            /* transition: color 0.3s ease; Animasi perubahan warna */
        }

        .text-primary-color:hover {
            color: #963567;
            /* Membuat warna sedikit lebih gelap (10% lebih gelap) */
        }
    </style>
</head>

<body class="font-body">

    <div style="min-height: 100vh" class="antialiased bg-gray-50 dark:bg-gray-900">

        @include('dashboard.layouts.nav')
        @include('dashboard.layouts.sidebar')

        <main id="container-main" class="">
            <div style="min-height: 100vh" id="main-section" class="p-4 md:ml-64 h-auto pt-20 border overflow-hidden">
                @yield('container')
            </div>
            <div id="footer-section" class="md:ml-64 h-auto relative">
                @include('dashboard.layouts.footer')
            </div>
        </main>


    </div>
    {{-- <script>
    const btnDeleteImage = document.getElementById('delete-image');
    console.log('okeee');

    btnDeleteImage.addEventListener('click', function () {
        if (confirm('Anda yakin ingin menghapus?')) {
            const foto = this.getAttribute('data-foto');

            fetch(`/delete_foto/?foto=${foto}`, {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json',
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
            })
            .then(response => {
                if (response.status === 200) {
                    window.location.href = "/";
                }
            })
            .catch(error => {
                console.error('Terjadi kesalahan:', error);
            });
        }
    });
</script> --}}
    <script src="js/script.js"></script>
</body>

</html>
