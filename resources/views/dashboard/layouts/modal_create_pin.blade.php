

<!-- Modal toggle -->
{{-- <button data-modal-target="authentication-modal" data-modal-toggle="authentication-modal" class="block text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800" type="button">
    Toggle modal
  </button> --}}

  <style>
    /* Menyembunyikan karakter input */
    .password-input {
        -webkit-text-security: disc; /* Untuk browser berbasis WebKit (Chrome, Safari, dll.) */
        text-security: disc; /* Untuk browser lainnya */
    }
</style>

  <!-- Main modal -->
  <div id="authentication-modal" tabindex="-1" aria-hidden="true" class="flex overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0 z-50 justify-center items-center w-full md:inset-0 h-[calc(100%-1rem)] max-h-full">
      <div class="relative p-4 w-full max-w-md max-h-full">
          <!-- Modal content -->
          <div class="relative bg-white rounded-lg shadow dark:bg-gray-700">
              <!-- Modal header -->
              <div class="flex items-center justify-between p-4 md:p-5 border-b rounded-t dark:border-gray-600">
                 <div>
                    <h3 class="text-xl font-semibold text-gray-900 dark:text-white">
                        Buat pin kamu
                      </h3>
                      {{-- <span class="text-sm text-gray-500">Pin ini akan kamu gunakan setiap kamu login</span> --}}
                 </div>
                  {{-- <button type="button" class="end-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm w-8 h-8 ms-auto inline-flex justify-center items-center dark:hover:bg-gray-600 dark:hover:text-white" data-modal-hide="authentication-modal">
                      <svg class="w-3 h-3" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 14 14">
                          <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6"/>
                      </svg>
                      <span class="sr-only">Close modal</span>
                  </button> --}}
              </div>
              <!-- Modal body -->
              <div class="p-4 md:px-5 md:py-4 pb-10">
                <form class="space-y-2" action="/pins" method="POST">
                    @error('user_id')
                    <p class="text-red-500 text-sm mt-1">{{ $message }}</p>
                    @enderror
                    @csrf
                    <input type="hidden" name="user_id" value="{{auth()->user()->id}}">
                    <div>
                        <label for="pin" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Masukkan PIN kamu</label>
                        <input type="text" name="pin" id="pin" class="password-input bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white" placeholder="123456" required>
                        @error('pin')
                            <p class="text-red-500 text-sm mt-1">{{ $message }}</p>
                        @enderror
                    </div>
                    <div>
                        <label for="confirm_pin" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Konfirmasi PIN kamu</label>
                        <input type="text" name="confirm_pin" id="confirm_pin" class="password-input bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white" placeholder="123456" required>
                        @error('confirm_pin')
                            <p class="text-red-500 text-sm mt-1">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="">
                        <span class="text-sm text-gray-500">PIN ini akan kamu gunakan setiap kamu login</span>
                    </div>
                    <button type="submit" class="w-full primary-color text-white focus:ring-4 focus:outline-none font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:hover:bg-blue-700 dark:focus:ring-blue-800">Buat PIN</button>
                </form>
            </div>

          </div>
      </div>
  </div>
  <div modal-backdrop="" class="bg-gray-900 bg-opacity-50 dark:bg-opacity-80 fixed inset-0 z-40"></div>
