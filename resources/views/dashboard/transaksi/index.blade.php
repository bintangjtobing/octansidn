@extends('dashboard.layouts.main')

@section('container')
    {{-- <div class="bg-white overflow-x-auto shadow-sm border border-gray-200 rounded-lg border-gray-300 dark:border-gray-600 h-fit mb-32 lg:mb-24"> --}}
      @if (request()->is('transaksi'))
      @include('dashboard.transaksi.layouts.tabel_transaksi')
      @endif

    <div class="{{(session()->has('sucses_verify_pin') || session()->has('verify-pin')) ? 'hidden' : ''}}">
        @include('dashboard.layouts.modal_verify_pin')
    </div>

    <div class="{{(session()->has('no_pins')) ? '' : 'hidden'}}">
        @include('dashboard.layouts.modal_create_pin')
    </div>

    {{-- </div> --}}

  <script src="js/transaksi_script.js"></script>
@endsection
