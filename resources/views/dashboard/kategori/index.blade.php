@extends('dashboard.layouts.main')

@section('container')
  {{-- <div class="bg-white shadow-sm border border-gray-200 rounded-lg border-gray-300 dark:border-gray-600 h-fit mb-4"> --}}
    @include('dashboard.kategori.layouts.tabel_categori_transaksi')
  {{-- </div> --}}
    <div class="{{(session()->has('no_pins')) ? '' : 'hidden'}}">
        @include('dashboard.layouts.modal_create_pin')
    </div>
    <div class="{{(session()->has('sucses_verify_pin') || session()->has('verify-pin')) ? 'hidden' : ''}}">
        @include('dashboard.layouts.modal_verify_pin')
    </div>

<script src="js/kategori_transaksi_script.js"></script>
@endsection
