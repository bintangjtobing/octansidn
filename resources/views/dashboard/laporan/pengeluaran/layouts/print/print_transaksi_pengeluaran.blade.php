<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laporan Pengeluaran</title>
    <script src="https://cdn.tailwindcss.com"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/flowbite/2.0.0/flowbite.min.css" rel="stylesheet" />
</head>
<body>
    <div>
        <h1 class="mb-5 text-2xl font-semibold text-center">Laporan Pengeluaran {{($bulan != null) ? 'Bulan ' .$bulan : ''}}</h1>
    </div>
    <section>
        <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
            <thead style="background: #F3F4F6" class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                <tr>
                    <th scope="col" class="px-4 py-3 border" align="left">no transaksi</th>
                    <th scope="col" class="px-4 py-3 border" align="left">Tanggal</th>
                    <th scope="col" class="px-4 py-3 border" align="left">Deskripsi</th>
                    <th scope="col" class="px-4 py-3 border" align="right">Jumlah</th>
                </tr>
            </thead>
            <tbody id="tabelBody">
                @foreach ($data as $item)
                <tr id="tabelRowPem" class="border-b dark:border-gray-700">
                    <th scope="row" align="left"
                        class="px-4 py-3 font-medium text-gray-900 border whitespace-nowrap dark:text-white">
                        {{$item->no_transaksi}}</th>
                    <td class="px-4 py-3 border" align="left">{{$item->tanggal}}</td>
                    <td class="px-4 py-3 border" align="left">{{$item->deskripsi ?? 'N/A'}}<br><span class="text-xs text-muted" style="color: gray">Kategori: {{$item->kategori_transaksi->nama}}</span></td>
                    <td class="px-4 py-3 border" align="right">Rp {{ number_format($item->jumlah, 0, ',', '.') }}</td>
                </tr>
                @endforeach
                <tr style="background: #F3F4F6" class="border-b dark:border-gray-700">
                    <th scope="row" align="right" colspan="3" class="px-4 py-3 border">Jumlah Total</th>
                    <td class="px-4 py-3 font-semibold border" align="right">Rp {{number_format($pengeluaran, 0, ',', '.')}}</td>
                </tr>
            </tbody>
        </table>
    </section>
<script src="https://cdnjs.cloudflare.com/ajax/libs/flowbite/2.0.0/flowbite.min.js"></script>
</body>
</html>
