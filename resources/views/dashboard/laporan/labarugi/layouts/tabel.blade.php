<section class="bg-gray-50 dark:bg-gray-900 mb-28">
    <div class="mx-auto">
        <!-- Start coding here -->
        <div class="bg-white dark:bg-gray-800 rounded-md relative shadow-sm border border-gray-200  sm:rounded-lg overflow-hidden">
            <div class="flex flex-col md:flex-row items-center justify-between space-y-3 md:space-y-0 md:space-x-4 p-4">
            </div>
            <div class="overflow-x-auto">
                <table class="min-w-full w-max 2xl:w-full text-sm text-left text-gray-500 dark:text-gray-400">
                    <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                        <tr>
                            <th scope="col" class="px-4 py-3">Nama Akun</th>
                            <th scope="col" class="px-4 py-3 text-right">Jumlah</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="border-b dark:border-gray-700">
                            <th scope="row"
                                class="px-4 py-3 font-medium text-gray-900 whitespace-nowrap dark:text-white">*
                                Pemasukan</th>
                            <td class="px-4 py-3 text-base text-right">Rp {{ number_format($pemasukan, 0, ',', '.') }}
                            </td>
                        </tr>
                        @foreach ($pemasukanByKategori as $item)
                            <tr class="border-b dark:border-gray-700">
                                <th scope="row"
                                    class="px-4 py-3 font-medium text-gray-900 whitespace-nowrap dark:text-white pl-10">
                                    - @if (strlen($item->nama) <= 19)
                                        {{ $item->nama }}
                                    @else
                                        {{ Str::limit($item->nama, 19) }}
                                    @endif
                                </th>
                                <td class="px-4 py-3 text-base text-right">Rp
                                    {{ number_format($item->jumlah, 0, ',', '.') }}</td>
                            </tr>
                        @endforeach
                        <tr class="border-b bg-gray-100 dark:border-gray-700">
                            <th scope="row"
                                class="px-4 py-3 font-semibold text-gray-900 whitespace-nowrap dark:text-white">TOTAL
                                PENDAPATAN</th>
                            <td class="px-4 py-3 text-base text-gray-800 font-semibold text-right">Rp
                                {{ number_format($pemasukan, 0, ',', '.') }}</td>
                        </tr>

                        <tr class="border-b dark:border-gray-700">
                            <th scope="row"
                                class="px-4 py-3 font-medium text-gray-900 whitespace-nowrap dark:text-white">*
                                Pengeluaran</th>
                            <td class="px-4 py-3 text-base text-right">Rp {{ number_format($pengeluaran, 0, ',', '.') }}
                            </td>
                        </tr>
                        @foreach ($pengeluaranByKategori as $item)
                            <tr class="border-b dark:border-gray-700">
                                <th scope="row"
                                    class="px-4 py-3 font-medium text-gray-900 whitespace-nowrap dark:text-white pl-10">
                                    - @if (strlen($item->nama) <= 19)
                                        {{ $item->nama }}
                                    @else
                                        {{ Str::limit($item->nama, 19) }}
                                    @endif
                                </th>
                                <td class="px-4 py-3 text-base text-right">Rp
                                    {{ number_format($item->jumlah, 0, ',', '.') }}</td>
                            </tr>
                        @endforeach
                        <tr class="border-b dark:border-gray-700">
                            <th scope="row"
                                class="px-4 py-3 font-medium text-gray-900 flex items-center gap-2 whitespace-nowrap dark:text-white pl-10">
                                Pajak Pemasukan

                                <svg data-tooltip-target="tooltip-bottom-pemasukan" data-tooltip-placement="bottom"
                                    class="w-4 h-4 text-gray-500 dark:text-white" aria-hidden="true"
                                    xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 20">
                                    <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"
                                        stroke-width="2"
                                        d="M8 9h2v5m-2 0h4M9.408 5.5h.01M19 10a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z" />
                                </svg>

                                <div id="tooltip-bottom-pemasukan" role="tooltip"
                                    class="absolute z-10 invisible inline-block px-3 py-2 text-xs font-medium text-white bg-gray-900 rounded-lg shadow-sm border border-gray-200 opacity-0 tooltip dark:bg-gray-700">
                                    Pajak pemasukan adalah pajak yang dikenakan atas penghasilan yang diterima oleh
                                    orang pribadi atau badan dikali dengan nilai 10%<br>Penghasilan yang dimaksud
                                    dapat
                                    berupa jenis transaksi pendapatan tetap, pendapatan tidak tetap dan lain-lain.
                                    <div class="tooltip-arrow" data-popper-arrow></div>
                                </div>
                            </th>
                            <td class="px-4 py-3 text-base text-right">
                                Rp {{ number_format($pajakPemasukan, 0, ',', '.') }}
                            </td>
                        </tr>
                        <tr class="border-b dark:border-gray-700">
                            <th scope="row"
                                class="px-4 py-3 font-medium text-gray-900 flex items-center gap-2 whitespace-nowrap dark:text-white pl-10">
                                Pajak Pengeluaran

                                <svg data-tooltip-target="tooltip-bottom-pengeluaran" data-tooltip-placement="bottom"
                                    class="w-4 h-4 text-gray-500 dark:text-white" aria-hidden="true"
                                    xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 20">
                                    <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"
                                        stroke-width="2"
                                        d="M8 9h2v5m-2 0h4M9.408 5.5h.01M19 10a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z" />
                                </svg>

                                <div id="tooltip-bottom-pengeluaran" role="tooltip"
                                    class="absolute z-10 invisible inline-block px-3 py-2 text-xs font-medium text-white bg-gray-900 rounded-lg shadow-sm border border-gray-200 opacity-0 tooltip dark:bg-gray-700">
                                    Pajak pengeluaran adalah pajak yang dikenakan atas pengeluaran yang dilakukan oleh
                                    orang pribadi atau badan dikali dengan nilai 11%<br>Pengeluaran yang dimaksud
                                    dapat
                                    berupa dari jenis transaksi pengeluaran pokok, pengeluaran tambahan dan lain-lain.
                                    <div class="tooltip-arrow" data-popper-arrow></div>
                                </div>
                            </th>
                            <td class="px-4 py-3 text-base text-right">
                                Rp {{ number_format($pajakPengeluaran, 0, ',', '.') }}
                            </td>
                        </tr>
                        <tr class="border-b bg-gray-100 dark:border-gray-700">
                            <th scope="row"
                                class="px-4 py-3 font-semibold text-gray-900 whitespace-nowrap dark:text-white">TOTAL
                                PENGELUARAN SEBELUM PAJAK</th>
                            <td class="px-4 py-3 text-base text-gray-800 font-semibold text-right">Rp
                                {{ number_format($pengeluaran, 0, ',', '.') }}</td>
                        </tr>
                        <tr class="border-b bg-gray-100 dark:border-gray-700">
                            <th scope="row"
                                class="px-4 py-3 font-semibold text-gray-900 whitespace-nowrap dark:text-white">TOTAL
                                PENGELUARAN SETELAH PAJAK</th>
                            <td class="px-4 py-3 text-base text-gray-800 font-semibold text-right">Rp
                                {{ number_format($pengeluaranSetalahPajak, 0, ',', '.') }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
