<section class="bg-gray-50 dark:bg-gray-900 mb-32 lg:mb-28">
    <div class="">
        <!-- Start coding here -->
        <div
            class="bg-white rounded-md overflow-visible dark:bg-gray-800 relative shadow-sm border border-gray-200  sm:rounded-lg overflow-hidden">
            <div class="flex flex-col md:flex-row items-center justify-between space-y-3 md:space-y-0 md:space-x-4 p-4">
                <div class="w-full md:w-1/2">

                </div>
                <div
                    class="flex flex-col md:flex-row space-y-2 md:space-y-0 items-stretch md:items-center justify-end md:space-x-3 flex-shrink-0">
                    <div class="grid grid-cols-3 gap-3 md:grid-cols-4">
                        <button id="actionsDropdownButton" data-dropdown-toggle="actionsDropdown"
                            class="w-full md:w-auto border flex items-center justify-center py-2 px-4 text-sm font-medium text-gray-900 focus:outline-none bg-white rounded-lg  hover:bg-gray-100 hover:text-[#9345a3] focus:z-10 focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-700 dark:bg-gray-800 dark:text-gray-400 dark:border-gray-600 dark:hover:text-white dark:hover:bg-gray-700"
                            type="button">
                            <svg class="-ml-1 mr-1.5 w-5 h-5" fill="currentColor" viewbox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg" aria-hidden="true">
                                <path clip-rule="evenodd" fill-rule="evenodd"
                                    d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" />
                            </svg>
                            Actions
                        </button>
                        <div id="actionsDropdown"
                            class="hidden z-50 w-44 bg-white rounded divide-y divide-gray-100 shadow dark:bg-gray-700 dark:divide-gray-600">
                            <ul class="py-1 text-sm text-gray-700 dark:text-gray-200"
                                aria-labelledby="actionsDropdownButton">
                                @if (auth()->user()->can('cetak pemasukan'))
                                <li onclick="" id="filterKategori" data-id-2="1" data-id="all">
                                    <a id="linkPrint" href="/pdf_laporan_pemasukan/?id=all"
                                        class="block py-2 px-4 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white">Print</a>
                                </li>
                                <li onclick="" id="filterKategori" data-id-2="1" data-id="all">
                                    <a id="linkExcel" href="/pemasukan_xlsx/?id=all"
                                        class="block py-2 px-4 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white">Export
                                        to excel</a>
                                </li>
                                @else
                                <li class="hidden" onclick="" id="filterKategori" data-id-2="1" data-id="all">
                                    <a id="linkPrint" href="/pdf_laporan_pemasukan/?id=all"
                                        class="block py-2 px-4 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white">Print</a>
                                </li>
                                <li class="hidden" onclick="" id="filterKategori" data-id-2="1" data-id="all">
                                    <a id="linkExcel" href="/pemasukan_xlsx/?id=all"
                                        class="block py-2 px-4 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white">Export
                                        to excel</a>
                                </li>
                                <li onclick="" id="filterKategori" data-id-2="1" data-id="all">
                                    <a id="" href="#"
                                        class="block py-2 px-4 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white">Print</a>
                                </li>
                                <li onclick="" id="filterKategori" data-id-2="1" data-id="all">
                                    <a id="" href="#"
                                        class="block py-2 px-4 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white">Export
                                        to excel</a>
                                </li>
                                @endif
                            </ul>
                        </div>

                        <button id="kategoriDropdownButton" data-dropdown-toggle="kategoriDropdown"
                            class="w-full md:w-auto border flex items-center justify-center py-2 px-4 text-sm font-medium text-gray-900 focus:outline-none bg-white rounded-lg  hover:bg-gray-100 hover:text-[#9345a3] focus:z-10 focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-700 dark:bg-gray-800 dark:text-gray-400 dark:border-gray-600 dark:hover:text-white dark:hover:bg-gray-700"
                            type="button">
                            <svg class="-ml-1 mr-1.5 w-5 h-5" fill="currentColor" viewbox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg" aria-hidden="true">
                                <path clip-rule="evenodd" fill-rule="evenodd"
                                    d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" />
                            </svg>
                            Kategori
                        </button>
                        <div id="kategoriDropdown"
                            class="hidden  w-44 bg-white rounded divide-y divide-gray-100 shadow dark:bg-gray-700 dark:divide-gray-600">
                            <ul class="py-1 text-sm text-gray-700 dark:text-gray-200"
                                aria-labelledby="kategoriDropdownButton">
                                <li onclick="acttionFilter(this)" id="filterKategori" data-id="all" data-id-2="1">
                                    <a
                                        class="block cursor-pointer py-2 px-4 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white">Semua</a>
                                </li>
                                @foreach ($kategori as $item)
                                <li onclick="acttionFilter(this)" id="filterKategori" data-id="{{$item->id}}"
                                    data-id-2="1">
                                    <a
                                        class="block cursor-pointer py-2 px-4 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white">{{$item->nama}}</a>
                                </li>
                                @endforeach
                            </ul>
                        </div>


                        <button id="actionsDropdownButton" data-dropdown-toggle="tahunDropdown"
                            class="w-full md:w-auto flex border items-center justify-center py-2 px-4 text-sm font-medium text-gray-900 focus:outline-none bg-white rounded-lg  hover:bg-gray-100 hover:text-[#9345a3] focus:z-10 focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-700 dark:bg-gray-800 dark:text-gray-400 dark:border-gray-600 dark:hover:text-white dark:hover:bg-gray-700"
                            type="button">
                            <svg class="-ml-1 mr-1.5 w-5 h-5" fill="currentColor" viewbox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg" aria-hidden="true">
                                <path clip-rule="evenodd" fill-rule="evenodd"
                                    d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" />
                            </svg>
                            Tahun
                        </button>
                        <div id="tahunDropdown"
                            class="hidden z-50 w-44 bg-white rounded divide-y divide-gray-100 shadow dark:bg-gray-700 dark:divide-gray-600">
                            <ul class="py-1 text-sm text-gray-700 dark:text-gray-200"
                                aria-labelledby="actionsDropdownButton">
                                <li onclick="yearsFilter(this)" id="filterKategori" data-id-2="1" data-id="all">
                                    <a
                                        class="block py-2 px-4 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white">Semua</a>
                                </li>
                                @foreach ($dataTahun as $item)
                                <li onclick="yearsFilter(this)" id="filterKategori" data-id-2="1"
                                    data-id="{{$item->tahun_transaksi}}">
                                    <a
                                        class="block py-2 px-4 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white">{{$item->tahun_transaksi}}</a>
                                </li>
                                @endforeach
                            </ul>
                        </div>


                        <button id="periodeBulanDropdownButton" data-dropdown-toggle="periodeDropdown"
                            class="w-full md:w-auto flex border items-center justify-center py-2 px-4 text-sm font-medium text-gray-900 focus:outline-none bg-white rounded-lg  hover:bg-gray-100 hover:text-[#9345a3] focus:z-10 focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-700 dark:bg-gray-800 dark:text-gray-400 dark:border-gray-600 dark:hover:text-white dark:hover:bg-gray-700"
                            type="button">
                            <svg class="-ml-1 mr-1.5 w-5 h-5" fill="currentColor" viewbox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg" aria-hidden="true">
                                <path clip-rule="evenodd" fill-rule="evenodd"
                                    d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" />
                            </svg>
                            Periode
                        </button>
                        <div id="periodeDropdown"
                            class="hidden z-50 w-44 bg-white rounded divide-y divide-gray-100 shadow dark:bg-gray-700 dark:divide-gray-600">
                            <ul id="container-list-item" class="py-1 text-sm text-gray-700 dark:text-gray-200"
                                aria-labelledby="actionsDropdownButton">
                                <li onclick="acttionPeriode(this)" id="filterKategori" data-id-2="1" data-id="all">
                                    <a
                                        class="block py-2 px-4 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white">Semua</a>
                                </li>
                                @foreach ($dataBulan as $item)
                                <li onclick="acttionPeriode(this)" id="filterKategori" data-id-2="1"
                                    data-id="{{$item->id_bulan}}">
                                    <a
                                        class="block py-2 px-4 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white">{{$item->bulan_transaksi}}</a>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="overflow-x-auto">
                <table class="w-max lg:w-full text-sm text-left text-gray-500 dark:text-gray-400">
                    <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                        <tr>
                            <th scope="col" class="px-4 py-3">no transaksi</th>
                            <th scope="col" class="px-4 py-3">Tanggal</th>
                            <th scope="col" class="px-4 py-3">kategori</th>
                            <th scope="col" class="px-4 py-3">costumer/supplier</th>
                            <th scope="col" class="px-4 py-3">Deskripsi</th>
                            <th scope="col" class="px-4 py-3">Jumlah</th>
                        </tr>
                    </thead>
                    <tbody id="tabelBody">
                        @foreach ($data as $item)
                        <tr id="tabelRowPem" class="border-b dark:border-gray-700">
                            <th scope="row"
                                class="px-4 py-3 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                {{$item->no_transaksi}}</th>
                            <td class="px-4 py-3">{{$item->tanggal}}</td>
                            <td class="px-4 py-3">{{$item->kategori_transaksi->nama}}</td>
                            <td class="px-4 py-3">{{$item->suppliers_or_customers->nama_bisnis ?? '--'}}</td>
                            <td class="px-4 py-3">{{$item->deskripsi ?? '--'}}</td>
                            <td class="px-4 py-3">Rp {{ number_format($item->jumlah, 0, ',', '.') }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <nav id="nav-pagination"
                class="flex flex-col md:flex-row justify-between items-start md:items-center space-y-3 md:space-y-0 p-4"
                aria-label="Table navigation">

                <div id="pagination" class="grid grid-cols-2 md:flex items-center gap-3">
                    <!-- Previous Button -->
                    <a href="{{$data->previousPageUrl() ?? '#'}}"
                        class="flex items-center justify-center px-4 h-10 text-base font-medium text-gray-500 bg-white border border-gray-300 rounded-lg hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white">
                        Sebelumnya
                    </a>

                    <!-- Next Button -->
                    <a id="next-url" href="{{$data->nextPageUrl()}}"
                        class="flex items-center justify-center px-4 h-10 text-base font-medium text-gray-500 bg-white border border-gray-300 rounded-lg hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white">
                        Selanjutnya
                    </a>
                    <span class="text-sm flex gap-1 text-gray-700 dark:text-gray-400">
                        Menampilkan<span class="font-semibold text-gray-900 dark:text-white">{{$data->firstItem()}}</span> hingga <span class="font-semibold text-gray-900 dark:text-white">{{$data->lastItem()}}</span> dari <span class="font-semibold text-gray-900 dark:text-white">{{$data->total()}}</span> Enteri
                    </span>
                </div>

                {{-- {{ $data->links() }} --}}
            </nav>
        </div>
    </div>
</section>
