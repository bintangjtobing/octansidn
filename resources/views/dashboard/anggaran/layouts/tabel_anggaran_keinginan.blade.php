@include('dashboard.anggaran.layouts.create_anggaran')
@include('dashboard.anggaran.layouts.update_anggaran')

<section class="col-span-2">
    <div class="mx-auto w-full">
        <!-- Start coding here -->
        <div id="container-table"
            class="bg-white min-h-[290px] dark:bg-gray-800 relative shadow-sm border border-gray-200  px-4 sm:rounded-lg overflow-hidden">
            <div class="flex flex-col md:flex-row p-4">
                <h3 class="text-xl lg:text-2xl font-medium">Anggaran Keinginan</h3>
            </div>
            <div class="overflow-x-auto">
                <table class="min-w-full w-max 2xl:w-full text-sm text-left text-gray-500 dark:text-gray-400">
                    <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                        <tr>
                            <th scope="col" class="px-4 py-3">Kategori Anggaran</th>
                            <th scope="col" class="px-4 py-3">Kategori Budgeting</th>
                            <th scope="col" class="px-4 py-3">Sisa Anggaran</th>
                            <th scope="col" class="px-4 py-3 text-right">Jumlah</th>
                            <th scope="col" class="px-4 py-3"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($AnggaranKeinginan as $index => $a)
                            <tr id="tabel-row" class="border-b dark:border-gray-700">

                                <td class="px-4 py-3">{{ Str::limit($a->kategori_transaksi->nama, 15, '...') }}</td>
                                <td class="px-4 py-3">
                                    {{ $a->kategori_anggaran->nama ?? '--' }}
                                </td>

                                <td class="px-4 py-3">
                                    <div class="flex items-center gap-3">
                                        <div class="w-full bg-gray-200 rounded-full h-1.5 dark:bg-gray-700">
                                            <div class="primary-color h-1.5 rounded-full dark:bg-blue-500" style="width: {{(round($a->persentase) < 100) ? round($a->persentase) : 100}}%"></div>
                                          </div>
                                        <P>{{ round($a->persentase) }}%</P>
                                    </div>
                                </td>

                                <td scope="row"
                                    class="px-4 py-3 font-medium text-gray-900 whitespace-nowrap dark:text-white text-right">
                                    Rp {{ number_format($a->jumlah, 0, ',', '.') }}
                                </td>
                                <td class="px-4 py-3 flex items-center justify-end">
                                    <div class="flex gap-5 mr-5">
                                        @if (auth()->user()->can('ubah anggaran'))
                                            <button data-id="{{ $a->id }}" id="updateProductButton">
                                                <svg class="w-5 h-5 text-gray-500 dark:text-white" aria-hidden="true"
                                                    xmlns="http://www.w3.org/2000/svg" fill="none"
                                                    viewBox="0 0 21 21">
                                                    <path stroke="currentColor" stroke-linecap="round"
                                                        stroke-linejoin="round" stroke-width="2"
                                                        d="M7.418 17.861 1 20l2.139-6.418m4.279 4.279 10.7-10.7a3.027 3.027 0 0 0-2.14-5.165c-.802 0-1.571.319-2.139.886l-10.7 10.7m4.279 4.279-4.279-4.279m2.139 2.14 7.844-7.844m-1.426-2.853 4.279 4.279" />
                                                </svg>
                                            </button>
                                        @else
                                            <button disabled data-id="{{ $a->id }}" id="updateProductButton">
                                                <svg class="w-5 h-5 text-gray-500 dark:text-white" aria-hidden="true"
                                                    xmlns="http://www.w3.org/2000/svg" fill="none"
                                                    viewBox="0 0 21 21">
                                                    <path stroke="currentColor" stroke-linecap="round"
                                                        stroke-linejoin="round" stroke-width="2"
                                                        d="M7.418 17.861 1 20l2.139-6.418m4.279 4.279 10.7-10.7a3.027 3.027 0 0 0-2.14-5.165c-.802 0-1.571.319-2.139.886l-10.7 10.7m4.279 4.279-4.279-4.279m2.139 2.14 7.844-7.844m-1.426-2.853 4.279 4.279" />
                                                </svg>
                                            </button>
                                        @endif
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <nav class="flex flex-col md:flex-row justify-between items-start md:items-center space-y-3 md:space-y-0 p-4"
                aria-label="Table navigation">
                {{-- {{$kategori_transaksi->links()}} --}}
            </nav>
        </div>
    </div>
</section>


<script>
    const btnDelete = document.getElementById('btn-delete')
    // delete
    btnDelete.addEventListener('click', function() {
        if (confirm('Anda yakin ingin melakukan tindakan ini??')) {
            this.setAttribute('data-id', dataId)
            const idDelete = this.getAttribute('data-id')

            const xhr = new XMLHttpRequest()

            xhr.onreadystatechange = () => {
                if (xhr.status === 200 && xhr.readyState === 4) {
                    window.location.href = "/anggaran";
                }
            }

            xhr.open('DELETE', '/anggaran/?id=' + this.getAttribute('data-id'), true)
            xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            xhr.setRequestHeader('X-CSRF-TOKEN', '{{ csrf_token() }}');
            xhr.send()

        }
    })
</script>
