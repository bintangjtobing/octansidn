<section class="">
    <div class="mx-auto max-w-screen-xl">
        <!-- Start coding here -->
        <div class="bg-white dark:bg-gray-800 relative overflow-hidden">
            <div class="overflow-x-auto">
                <table class="min-w-full w-max 2xl:w-full text-base text-left text-gray-500 dark:text-gray-400">
                    <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                        <tr>
                            <th scope="col" class="px-4 py-3 text-base">Kategori</th>
                            <th scope="col" class="px-4 py-3 text-base">jumlah</th>
                        </tr>
                    </thead>
                    <tbody id="tabel-body">
                        @foreach ($topPengeluaran as $index => $item)
                            <tr id="tabel-row" class="border-b dark:border-gray-700">
                                <td class="px-4 py-3 text-red-600">
                                    @if (strlen($item['nama']) <= 15)
                                        {{ $item['nama'] }}
                                    @else
                                        {{ Str::limit($item['nama'], 15) }}
                                    @endif
                                </td>
                                <td class="px-4 py-3 text-red-600">Rp
                                    {{ number_format($item['total_jumlah'], 0, ',', '.') }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
