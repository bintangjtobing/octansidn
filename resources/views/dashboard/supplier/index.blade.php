@extends('dashboard.layouts.main')

@section('container')
<div class="{{(session()->has('no_pins')) ? '' : 'hidden'}}">
    @include('dashboard.layouts.modal_create_pin')
</div>
<div class="{{(session()->has('sucses_verify_pin') || session()->has('verify-pin')) ? 'hidden' : ''}}">
    @include('dashboard.layouts.modal_verify_pin')
</div>

    @include('dashboard.supplier.layouts.tabel_supplier')
    @include('dashboard.supplier.layouts.update_modal')
    <div modal-backdrop="" id="modal-backdrop" class="hidden bg-gray-900 bg-opacity-50 dark:bg-opacity-80 fixed inset-0 z-40"></div>
<script src="./js/supCos/script.js"></script>
@endsection
