@include('dashboard.supplier.layouts.create_modal')
@include('dashboard.supplier.layouts.update_modal')



<section class="bg-gray-50 dark:bg-gray-900>
    <div class="">
        <!-- Start coding here -->
        <div class="bg-white dark:bg-gray-800 relative shadow-sm border border-gray-200  sm:rounded-lg overflow-hidden">
            <div class="flex flex-col md:flex-row items-center justify-between space-y-3 md:space-y-0 md:space-x-4 p-4">
                <div class="w-full md:w-1/2">
                    <form>
                        <div class="flex">
                            <label for="search-dropdown"
                                class="mb-2 text-sm font-medium text-gray-900 sr-only dark:text-white">Your
                                Email</label>
                            <div class="relative w-full border rounded-lg">
                                <input type="search" id="search-dropdown"
                                    class="block p-2.5 w-full z-20 text-sm text-gray-900 bg-gray-50 rounded-lg border-gray-50 border border border-gray-300 focus:ring-[#9345a3] focus:border-[#9345a3] dark:bg-gray-700 dark:border-l-gray-700  dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:border-[#9345a3]"
                                    required placeholder="Cari berdasarkan nama bisnis">
                                <button type="submit"
                                    class="absolute top-0 right-0 p-2.5 text-sm font-medium h-full text-white primary-color rounded-r-lg borderborder-[#9345a3] hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-[#9345a3] dark:focus:ring-blue-800">
                                    <svg class="filterTabel w-4 h-4" aria-hidden="true" xmlns="http://www.w3.org/2000/svg"
                                        fill="none" viewBox="0 0 20 20">
                                        <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"
                                            stroke-width="2" d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z" />
                                    </svg>
                                    <span class="sr-only">Search</span>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div
                    class="w-full md:w-auto flex flex-col md:flex-row space-y-2 md:space-y-0 items-stretch md:items-center justify-end md:space-x-3 flex-shrink-0">
                    @if (auth()->user()->can('tambah supplier'))
                    <button data-modal-toggle="create_sup_cos" type="button"
                    class="flex items-center justify-center text-white primary-color hover:bg-primary-800 focus:ring-4 focus:ring-[#9345a3] font-medium rounded-lg text-sm px-4 py-2 dark:bg-primary-600 dark:hover:primary-color focus:outline-none dark:focus:ring-[#9345a3]">
                    <svg class="h-3.5 w-3.5 mr-2" fill="currentColor" viewbox="0 0 20 20"
                            xmlns="http://www.w3.org/2000/svg" aria-hidden="true">
                            <path clip-rule="evenodd" fill-rule="evenodd"
                                d="M10 3a1 1 0 011 1v5h5a1 1 0 110 2h-5v5a1 1 0 11-2 0v-5H4a1 1 0 110-2h5V4a1 1 0 011-1z" />
                        </svg>

                    Tambah Supplier/Costumer
                </button>
                    @else
                    <button disabled data-modal-toggle="create_sup_cos" type="button"
                    class="flex items-center justify-center text-white primary-color hover:bg-primary-800 focus:ring-4 focus:ring-[#9345a3] font-medium rounded-lg text-sm px-4 py-2 dark:bg-primary-600 dark:hover:primary-color focus:outline-none dark:focus:ring-[#9345a3]">
                    <svg class="h-3.5 w-3.5 mr-2" fill="currentColor" viewbox="0 0 20 20"
                    xmlns="http://www.w3.org/2000/svg" aria-hidden="true">
                    <path clip-rule="evenodd" fill-rule="evenodd"
                        d="M10 3a1 1 0 011 1v5h5a1 1 0 110 2h-5v5a1 1 0 11-2 0v-5H4a1 1 0 110-2h5V4a1 1 0 011-1z" />
                </svg>
                    Tambah Supplier/Costumer
                </button>
                    @endif
                </div>
            </div>
            <div class="overflow-x-auto">
                <table class="min-w-full w-max 2xl:w-full text-sm text-left text-gray-500 dark:text-gray-400">
                    <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                        <tr>
                            <th scope="col" class="px-4 py-3">Nama bisnis</th>
                            <th scope="col" class="px-4 py-3">Tipe</th>
                            <th scope="col" class="px-4 py-3">Jenis Transaksi</th>
                            <th scope="col" class="px-4 py-3">No handphone</th>
                            <th scope="col" class="px-4 py-3">ALamat</th>
                            <th scope="col" class="px-4 py-3">Email</th>
                            <th scope="col" class="px-4 py-3">
                                <span class="sr-only">Actions</span>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $item)
                        <tr id="tabel-row" class="border-b dark:border-gray-700">
                            <th scope="row"
                                class="px-4 py-3 font-medium text-gray-900 whitespace-nowrap dark:text-white">{{$item->nama_bisnis}}</th>
                                <td class="px-4 py-3">{{($item->jenis_transaksi_id === 1 || $item->jenis_transaksi_id === 2) ? 'Costumer' : 'Supplier'}}</td>
                                <td class="px-4 py-3">{{$item->jenis_transaksi->nama}}</td>
                                <td class="px-4 py-3">{{$item->no_hp}}</td>
                            <td class="px-4 py-3">{{$item->alamat}}</td>
                            <td class="px-4 py-3">{{$item->email}}</td>
                            <td class="px-4 py-3 flex items-center justify-end">
                               <button data="{{$item->id}}" id="btnEditSup">
                                <svg class="w-6 h-6 text-gray-500 dark:text-white" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 20 18">
                                    <path d="M12.687 14.408a3.01 3.01 0 0 1-1.533.821l-3.566.713a3 3 0 0 1-3.53-3.53l.713-3.566a3.01 3.01 0 0 1 .821-1.533L10.905 2H2.167A2.169 2.169 0 0 0 0 4.167v11.666A2.169 2.169 0 0 0 2.167 18h11.666A2.169 2.169 0 0 0 16 15.833V11.1l-3.313 3.308Zm5.53-9.065.546-.546a2.518 2.518 0 0 0 0-3.56 2.576 2.576 0 0 0-3.559 0l-.547.547 3.56 3.56Z"/>
                                    <path d="M13.243 3.2 7.359 9.081a.5.5 0 0 0-.136.256L6.51 12.9a.5.5 0 0 0 .59.59l3.566-.713a.5.5 0 0 0 .255-.136L16.8 6.757 13.243 3.2Z"/>
                                  </svg>
                               </button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <nav class="flex flex-col md:flex-row justify-between items-start md:items-center space-y-3 md:space-y-0 p-4"
                aria-label="Table navigation">
                {{$data->links()}}
            </nav>
        </div>
    </div>

    <script>
        const btnEditSup = Array.from(document.querySelectorAll('#btnEditSup'))
const namaBisnis = document.getElementById('nama_bisnis_detail')
const alamatBisnis = document.getElementById('alamat_detail')
const emailBisnis = document.getElementById('email_detail')
const noHp = document.getElementById('no_hp')
const optTipe = Array.from(document.querySelectorAll('#optTipe'))
const btnDeleteSupCos = document.getElementById('btnDeleteSup')
const jnTransaksiSup = Array.from(document.querySelectorAll('#jnTransaksiSup'))

btnEditSup.forEach(btnEditSup => {
    btnEditSup.addEventListener('click', function() {
        // console.log(btnDeleteSupCos);
        modalUpdate.classList.replace('hidden', 'flex')
                                modalBackdrop.classList.toggle('hidden')
        fetch('get_sup_or_cos_by_no_hp/?id=' + this.getAttribute('data'))
            .then(response => {
                if(!response.ok) {
                    throw new Error('Network response was not ok');
                }

                return response.json()
            })
            .then(data => {
                data.forEach(item => {
                    namaBisnis.value = item.nama_bisnis
                    alamatBisnis.value = item.alamat
                    emailBisnis.value = item.email
                    noHp.value = item.no_hp
                    btnDeleteSupCos.setAttribute('data-id', item.id)
                    optTipe.forEach(itemOpt => {
                        if(item.jenis_transaksi_id == 1 || item.jenis_transaksi_id == 2) {
                            optTipe[0].selected = true
                        }else if(item.jenis_transaksi_id == 3 || item.jenis_transaksi_id == 4){
                            optTipe[1].selected = true
                        }
                    })
                    jnTransaksiSup.forEach((itemJnTran, index) => {
                        if(item.jenis_transaksi_id == itemJnTran.value){
                            itemJnTran.selected = true
                            console.log(itemJnTran.value);
                        }
                    })
                });

            })
            .catch(error => {
                console.error('fetch error', error)
            })
    })
})
    </script>
</section>

