<section class="mb-20">
    <div class="mx-auto">
        <!-- Start coding here -->
        <div class="bg-white dark:bg-gray-800 relative overflow-hidden">
            <div class="overflow-x-auto">
                <table class="min-w-full w-max 2xl:w-full text-base text-left text-gray-500 dark:text-gray-400">
                    <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                        <tr>
                            <th scope="col" class="px-4 py-3 text-base">Kategori</th>
                            <th scope="col" class="px-4 py-3 text-base">Jumlah</th>
                            <th scope="col" class="px-4 py-3 text-base">Waktu</th>
                        </tr>
                    </thead>
                    <tbody id="tabel-body">
                        @foreach ($transaksiTerkini as $item)
                            <tr id="tabel-row" class="border-b dark:border-gray-700">
                                <td style="color: {{ $item['jenis_transaksi_id'] == 1 || $item['jenis_transaksi_id'] == 2 ? '#057A55' : ($item['jenis_transaksi_id'] == 3  || $item['jenis_transaksi_id' == 4]? '#E02424' : '#1C64F2') }}"
                                    class="px-4 py-3 text-base">
                                    {{-- {{ substr($item['kategori_transaksi']->nama, 0, 25) ?? '--' }} --}}
                                    @if (strlen($item['kategori_transaksi']->nama ?? '--') <= 15)
                                        {{ $item['kategori_transaksi']->nama ?? '-- ' }}
                                    @else
                                        {{ Str::limit($item['kategori_transaksi']->nama, 15) }}
                                    @endif
                                </td>

                                <td scope="row"
                                    style="color: {{ $item['jenis_transaksi_id'] == 1 || $item['jenis_transaksi_id'] == 2 ? '#057A55' : ($item['jenis_transaksi_id'] == 3  || $item['jenis_transaksi_id' == 4]? '#E02424' : '#1C64F2') }}"
                                    class="px-4 py-3 text-base whitespace-nowrap dark:text-white">
                                    {{ $item['jenis_transaksi_id'] == 1 ? '+ Rp ' : '- Rp ' }}{{ number_format($item['jumlah'], 0, ',', '.') }}
                                </td>
                                <td scope="row"
                                    style="color: {{ $item['jenis_transaksi_id'] == 1 || $item['jenis_transaksi_id'] == 2 ? '#057A55' : ($item['jenis_transaksi_id'] == 3  || $item['jenis_transaksi_id' == 4]? '#E02424' : '#1C64F2') }}"
                                    class="px-4 py-3 text-base whitespace-nowrap dark:text-white">
                                    {{ $item['tanggal'] }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
